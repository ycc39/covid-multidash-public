## Dash/Flask dashboard for Covid modelling.

*Author/Maintainer: Yin-Chi Chan, Institute for Manufacturing,
Univ. of Cambridge*

This dashboard is part of a collaboration between the Institute for
Manufacturing, University of Cambridge and Cambridge University Hospitals.

**Note:** This dashboard uses Flask's built-in development server and is
intended for **internal use only**. For deployment, one should modify the
dashboard to use a production server, such as uWSGI.  Background tasks should
also be migrated from diskcache to Celery.

See:
- <https://flask.palletsprojects.com/en/2.2.x/deploying/>
- <https://dash.plotly.com/background-callbacks>

### Running the local webserver

1. Install Docker:
    - Windows: <https://docs.docker.com/desktop/install/windows-install/>
    - Linux: <https://docs.docker.com/desktop/install/linux-install/>
2. To build the webserver: `docker compose build`.  This only needs to be done before the first launch or when the code changes.
3. To launch the webserver: `docker compose up -d`
4. To shut down the webserver: `docker compose down`
