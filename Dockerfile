FROM python:3.11-slim

COPY . .

RUN pip install --no-cache-dir -U pip
RUN pip install --no-cache-dir pip-tools
RUN pip-compile requirements.in
RUN pip-sync requirements.txt --pip-args "--no-cache-dir"

WORKDIR /app

CMD ["flask", "run", "-h", "0.0.0.0", "-p", "5000", "--no-reload", "--no-debugger", "--without-threads"]
