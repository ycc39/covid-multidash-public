import dash_bootstrap_components as dbc
from dash import dash, dcc, html
from dash.dash_table import DataTable
from flask import Flask

from .callbacks import register_callbacks
from .controls import *
from .info import info


def create_app(server: Flask, path: str) -> dash.Dash:
    app = dash.Dash(
        'CUH Covid Multi-Dashboard',
        server=server,
        url_base_pathname=path,
        external_stylesheets=[dbc.themes.BOOTSTRAP, dbc.icons.BOOTSTRAP],
        suppress_callback_exceptions=True
    )
    app.layout = layout(app)
    register_callbacks(app)
    return app


ZOOM_X_MSG = html.P(
    'To zoom in on the x axis only, click and drag in a horizontal line. '
    'To pan, click and drag along the x axis labels, just below the plot. '
    'Double-clicking on the plot resets the zoom level.'
)


def layout(app: dash.Dash) -> html.Div:
    """Generates the app layout for the Analysis dashboard.

    Args:
        app (dash.Dash): The dashboard to populate with HTML and Dash elements.

    Returns:
        html.Div: The top-level div for the dashboard.
    """

    return html.Div(
        [
            html.H1('CUH Covid Multi-Dashboard: Patient Analysis'),
            html.Div(
                [
                    html.Div(
                        [
                            html.A(
                                [dbc.Button(
                                    'Home', id=TOP.BUTTON_HOME, className='btn btn-danger'
                                )],
                                href='/'
                            ),
                            dcc.Upload(
                                [dbc.Button('Upload Patient Details',
                                            className='btn btn-primary')],
                                id=TOP.UPLOAD,
                                multiple=False,
                                style={'width': 'fit-content',
                                       'display': 'inline'}
                            ),
                        ],
                        className='d-inline-flex gap-1'
                    ),
                    # raw data
                    dcc.Store(id=STORES.RAW),

                    # apply data_clean()
                    dcc.Store(id=STORES.CLEANED),
                    dcc.Store(id=STORES.CURRENT),

                    # apply data_clean() and adjust_dates()
                    dcc.Store(id=STORES.ADJUSTED),
                    dcc.Store(id=STORES.ADJUSTED_CURRENT),

                    html.Div(
                        dcc.Loading(
                            dbc.Alert(
                                [
                                    html.I(
                                        className='bi bi-exclamation-triangle-fill me-2'),
                                    'No file uploaded yet.'
                                ],
                                color="warning",
                                className="d-flex align-items-start"
                            ),
                            id=STATUS.UPLOAD,
                        )
                    ),

                    html.Div(
                        [
                            'Admission Date range (for tabs with italic labels): ',
                            dcc.DatePickerRange(
                                start_date='2023-01-01',
                                end_date='2023-03-01',
                                display_format='YYYY-MM-DD',
                                first_day_of_week=1,
                                minimum_nights=0,
                                id=INPUTS.DATE_RANGE,
                                disabled=True
                            )
                        ]
                    )
                ],
                className='d-grid gap-2 mb-2'
            ),
            dbc.Tabs([
                dbc.Tab(
                    [
                        dcc.Checklist(
                            ['Current Patients Only', 'ICU Patients Only'],
                            [],
                            id=INPUTS.VIEW_OPTIONS,
                            className="d-inline-flex gap-2"
                        ),
                        DataTable(
                            id=TABLES.DETAILS,
                            style_table={'width': '1600px',
                                         'overflow-x': 'auto'},
                            page_size=20
                        )
                    ],
                    label='View Data',
                    active_label_style={
                        'background-color': 'lightblue', 'font-weight': 'bold'}
                ),
                dbc.Tab(
                    [
                        dcc.Checklist(
                            ['Current Patients Only', 'ICU Patients Only'],
                            [],
                            id=INPUTS.VIEW_OPTIONS_ADJ,
                            className="d-inline-flex gap-2"
                        ),
                        DataTable(
                            id=TABLES.ADJUSTED,
                            style_table={'width': '1600px',
                                         'overflow-x': 'auto'},
                            page_size=20
                        )
                    ],
                    label='Adjusted Stay Dates',
                    active_label_style={
                        'background-color': 'lightblue', 'font-weight': 'bold'}
                ),
                dbc.Tab(
                    [
                        ZOOM_X_MSG,
                        dcc.Graph(
                            id=GRAPHS.DAILY_ARR,
                            style={'width': '100%', 'height': '450px'}
                        )
                    ],
                    label='Daily Arrivals',
                    active_label_style={
                        'background-color': 'lightblue', 'font-weight': 'bold'}
                ),
                dbc.Tab(
                    [
                        dcc.Graph(
                            id=GRAPHS.HOURLY_ARR,
                            style={'width': '900px', 'height': '450px'}
                        )
                    ],
                    label='Arrivals by Hour of Day',
                    label_style={'font-style': 'italic'},
                    active_label_style={
                        'background-color': 'lightblue', 'font-weight': 'bold'}
                ),
                dbc.Tab(
                    html.Div(
                        [
                            html.Div([
                                html.Strong(
                                    'Survival rates (including current patients):'),
                                DataTable(
                                    id=TABLES.SURVIVAL,
                                    style_table={'width': 'fit-content'}
                                ),
                            ]),

                            html.Div([
                                html.Strong(
                                    'ICU admission rates (% of all patients):'),
                                DataTable(
                                    id=TABLES.ICU,
                                    style_table={'width': 'fit-content'}
                                ),
                            ]),
                            html.Div([
                                html.Strong(
                                    'Pre-ICU stay in GIM (% of all ICU patients): '),
                                html.Span('{}%', id=NUMBER.PRE_ICU)
                            ])
                        ],
                        className='d-grid gap-4'
                    ),
                    label='Mortality',
                    label_style={'font-style': 'italic'},
                    active_label_style={
                        'background-color': 'lightblue', 'font-weight': 'bold'},
                    tab_class_name='d-grid gap-2'
                ),
                dbc.Tab(
                    html.Div(
                        [
                            html.Span([
                                html.Strong('Filter: '),
                                'remove long-stays (outliers) longer than ___ days. '
                                '(Leave blank to disable filter)',
                            ]),
                            html.Div(
                                [
                                    html.Strong('GIM-only'),
                                    dbc.Input(
                                        id=INPUTS.MAX_GIM,
                                        type="number",
                                        min=0,
                                        style={'width': '5em'}
                                    ),
                                    html.Strong('ICU'),
                                    dbc.Input(
                                        id=INPUTS.MAX_ICU,
                                        type="number",
                                        min=0,
                                        style={'width': '5em'}
                                    ),
                                    html.Strong('post-ICU'),
                                    dbc.Input(
                                        id=INPUTS.MAX_POST_ICU,
                                        type="number",
                                        min=0,
                                        style={'width': '5em'}
                                    )
                                ],
                                className='d-inline-flex gap-2 mb-4'
                            ),
                            html.Div([
                                html.Strong(
                                    'Fitted Length of Stay Distributions:'),
                                DataTable(
                                    id=TABLES.LOS,
                                    style_table={'width': 'fit-content'}
                                ),
                            ]),
                            dcc.Graph(
                                id=GRAPHS.LOS,
                                style={'width': '100%', 'height': '450px'}
                            ),
                        ],
                        className='d-grid gap-2'
                    ),
                    label='LoS',
                    label_style={'font-style': 'italic'},
                    active_label_style={
                        'background-color': 'lightblue', 'font-weight': 'bold'}
                ),
                dbc.Tab(
                    [
                        html.H4('LoS distribution fitting parameters'),
                        html.P('(Set these in the top menu and in the LoS tab.)'),
                        html.Ul([
                            html.Li([
                                html.Strong('Start date: '),
                                html.Span('yyyy-mm-dd', id=STATUS.START_DATE)
                            ]),
                            html.Li([
                                html.Strong('End date: '),
                                html.Span('yyyy-mm-dd', id=STATUS.END_DATE)
                            ]),
                            html.Li([
                                html.Strong('Max GIM stay (filter): '),
                                html.Span('(Unlimited)', id=STATUS.MAX_GIM)
                            ]),
                            html.Li([
                                html.Strong('Max ICU stay (filter): '),
                                html.Span('(Unlimited)', id=STATUS.MAX_ICU)
                            ]),
                            html.Li([
                                html.Strong('Max Pre-ICU stay (filter): '),
                                html.Span('(Unlimited)',
                                          id=STATUS.MAX_POST_ICU)
                            ])
                        ]),
                        dbc.Button(
                            'Download file',
                            id=DOWNLOAD.BUTTON,
                            disabled=True,
                            color='secondary',
                        ),
                        dcc.Download(id=DOWNLOAD.CONTENTS)
                    ],
                    label='Generate config file',
                    label_style={'font-style': 'italic'},
                    active_label_style={
                        'background-color': 'lightblue', 'font-weight': 'bold'}
                ),
                dbc.Tab(
                    info(app),
                    label='Info',
                    active_label_style={
                        'background-color': 'lightblue', 'font-weight': 'bold'}
                ),
            ]),
        ],
        className='d-grid',
        style={'margin': '1rem', 'width': '1600px'}
    )
