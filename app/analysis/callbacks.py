"""Declare callbacks for the Analysis dashboard."""

from base64 import b64decode
from io import BytesIO
from typing import List

import dash_bootstrap_components as dbc
import pandas as pd
import plotly.express as px
import plotly.graph_objs as go
from dash import dash, dcc, html
from dash.dependencies import Input, Output, State

from .cols import COLS
from .controls import *
from .daily_arrivals import get_daily_arrivals
from .gen_config_file import gen_config_file
from .hourly_arrivals import get_hourly_arrivals
from .los import get_los_info, plot_los_info
from .mortality import get_mortality_info
from .preprocessing import (adjust_dates, check_cols, current_patients,
                            data_clean, fill_missing_discharges)
from .table_styling import (ADJUSTED_TABLE_FORMATTING, ADJUSTED_TABLE_STYLE,
                            CURRENT_PATIENTS_TABLE_STYLE,
                            ICU_RATE_TABLE_FORMATTING, LOS_TABLE_FORMATTING,
                            SURVIVAL_TABLE_FORMATTING)

MISSING_COLS_ERROR_STR = \
    f"""Table must begin in Cell A1 of the first sheet and contain at least the following columns:

`{COLS}`
"""
"""Error string shown when the uploaded patient data file can be read,
but is missing vital columns."""


def register_callbacks(app: dash.Dash):
    """_summary_

    Args:
        app (dash.Dash): The app to which callbacks are registered.
    """
    print('Registering callbacks...')

    @app.callback(
        Output(STATUS.UPLOAD, 'children'),
        Output(STORES.RAW, 'data'),
        Output(STORES.CLEANED, 'data'),
        Output(STORES.CURRENT, 'data'),
        Output(STORES.ADJUSTED, 'data'),
        Output(STORES.ADJUSTED_CURRENT, 'data'),

        Input(TOP.UPLOAD, 'contents'),
        State(TOP.UPLOAD, 'filename'),

        prevent_initial_call=True
    )
    def upload_details(content: str, filename: str):
        """Callback for when a patient details file is uploaded to the dashboard.


        Args:
            content (str): The file as a base64 encoded string with metadata.
            filename (str): The filename of the uploaded file.

        Returns:
            tuple: tuple of Dash Outputs. In the case an error occurred, all
            connected Dash stores are cleared (set to None).
        """
        print('upload_details callback')
        if content is None:
            return (dash.no_update,) * 6
        try:
            if not (filename.endswith('.xls') or filename.endswith('.xlsx')):
                raise Exception('Uploaded file not an .xls or .xlsx file.')

            # file_contents: base64 encoded string
            _, file_contents = content.split(',')
            df = pd.read_excel(BytesIO(b64decode(file_contents)))
            if not check_cols(df):
                raise Exception(MISSING_COLS_ERROR_STR)
            df = df.loc[:, COLS]  # drop unnecessary columns
            df_cleaned = data_clean(df)
            df_current = current_patients(df_cleaned)
            df_revised = adjust_dates(fill_missing_discharges(df_cleaned))
            df_revised_current = df_revised.loc[df_revised.Censored |
                                                df_revised.Censored_ICU]
        except Exception as exception:
            # Wrap exception message in a dbc.Alert element
            return (
                dbc.Alert(
                    [
                        html.I(className='bi bi-x-octagon-fill me-2'),
                        dcc.Markdown(
                            str(exception),
                            # undo margin added by last <p> in dcc Markdown element
                            style={'margin-bottom': '-1em'}
                        ),
                    ],
                    color='danger',
                    className='d-flex align-items-start',
                    dismissable=False
                ),

                # no valid dataframe
                None, None, None, None, None
            )
        return (
            dbc.Alert(
                [
                    html.I(className='bi bi-check-circle-fill me-2'),
                    f' Successfully read and processed {len(df_cleaned)} rows.'
                ],
                color='success',
                className='d-flex align-items-start',
                dismissable=True
            ),

            # dataframe to dcc Store
            df.to_dict(orient='records'),
            df_cleaned.to_dict(orient='records'),
            df_current.to_dict(orient='records'),
            df_revised.to_dict(orient='records'),
            df_revised_current.to_dict(orient='records')
        )

    @app.callback(
        Output(TABLES.DETAILS, 'data'),
        Output(TABLES.DETAILS, 'style_data_conditional'),
        Input(STORES.CLEANED, 'data'),
        Input(STORES.CURRENT, 'data'),
        Input(INPUTS.VIEW_OPTIONS, 'value'),

        prevent_initial_call=True
    )
    def view_data(data: List | None, data_current: List | None, view_options: List[str]):
        """Populates the table in the View Data tab.

        Args:
        - data (List | None): Patient data from a dcc.Store object,
          after basic data cleaning.
        - data_current (List | None): Subset of the data for current patients only.
        - view_options (List[str]): Which checkboxes are selected, i.e. 'Current Patients
          Only' and/or 'ICU Patients Only'

        Returns:
        - 2-element list containing the table data and formatting information.
        """
        print('view_data callback')

        if data is None or data_current is None:
            return None, None

        if 'Current Patients Only' in view_options:
            data_ret = data_current
        else:
            data_ret = data

        if 'ICU Patients Only' in view_options:
            data_ret = pd.DataFrame(data_ret)
            data_ret = data_ret.loc[~pd.isna(data_ret.ICU_Admission)]
            data_ret = data_ret.to_dict('records')

        return data_ret, CURRENT_PATIENTS_TABLE_STYLE

    @app.callback(
        Output(TABLES.ADJUSTED, 'data'),
        Output(TABLES.ADJUSTED, 'columns'),
        Output(TABLES.ADJUSTED, 'style_data_conditional'),
        Input(STORES.ADJUSTED, 'data'),
        Input(STORES.ADJUSTED_CURRENT, 'data'),
        Input(INPUTS.VIEW_OPTIONS_ADJ, 'value'),

        prevent_initial_call=True
    )
    def view_data_adjusted(data: List | None, data_current: List | None, view_options: List[str]):
        """Populates the table in the Adjusted Stay Dates tab.

        Args:
        - data (list | None): Patient data from a dcc.Store object,
          after full preprocessing including adjustment of stay dates.
          See also `upload_details()` and `preprocessing.py`.
        - data_current (list | None): Subset of the data for current patients only.
        - view_options (list[str]): Which checkboxes are selected, i.e. 'Current Patients
          Only' and/or 'ICU Patients Only'

        Returns:
        - 2-element list containing the table data and formatting information.
        """
        print('view_data_adjusted callback')

        if data is None or data_current is None:
            return None, None, None

        if 'Current Patients Only' in view_options:
            data_ret = data_current
        else:
            data_ret = data

        if 'ICU Patients Only' in view_options:
            data_ret = pd.DataFrame(data_ret)
            data_ret = data_ret.loc[~pd.isna(data_ret.ICU_Admission)]
            data_ret = data_ret.to_dict('records')

        # Conditional formatting doesn't work on booleans, so convert to string
        # Possible to vectorize and/or automatically detect boolean columns?
        for i, _ in enumerate(data_ret):
            data_ret[i]['Censored'] = str(data_ret[i]['Censored']).lower()
            data_ret[i]['Censored_ICU'] = str(
                data_ret[i]['Censored_ICU']).lower()
            data_ret[i]['Censored_postICU'] = str(
                data_ret[i]['Censored_postICU']).lower()

        return data_ret, ADJUSTED_TABLE_FORMATTING, ADJUSTED_TABLE_STYLE

    @app.callback(
        Output(INPUTS.DATE_RANGE, 'min_date_allowed'),
        Output(INPUTS.DATE_RANGE, 'max_date_allowed'),
        Output(INPUTS.DATE_RANGE, 'start_date'),
        Output(INPUTS.DATE_RANGE, 'end_date'),
        Output(INPUTS.DATE_RANGE, 'disabled'),
        Input(STORES.ADJUSTED, 'data'),

        prevent_initial_call=True
    )
    def update_date_range(data: List | None):
        """Update the date range picker control when a new file is uploaded.

        Args:
        - data (List | None): Patient data from a dcc.Store object,
          after full preprocessing including adjustment of stay dates.
          See also `upload_details()` and `preprocessing.py`.

        Returns:
        1. The earliest allowed date for the date picker.
        2. The latest allowed date.
        3. The start date for the selected date range.
        4. The end date for the selected date range.
        5. Boolean representing whether the date range picker control
        should be disabled.
        """
        print('update_date_range callback')

        if data is None:
            return dash.no_update, dash.no_update, dash.no_update, dash.no_update, True
        df = pd.DataFrame(data)
        df.Admission = pd.to_datetime(df.Admission)
        df.Discharge = pd.to_datetime(df.Admission)
        min_day = str(min(df.Admission).floor(freq='D').date())
        max_day = str(max(df.Discharge).floor(freq='D').date())
        return min_day, max_day, min_day, max_day, False

    @app.callback(
        Output(GRAPHS.DAILY_ARR, 'figure'),
        Input(STORES.ADJUSTED, 'data'),
        prevent_initial_call=True
    )
    def plot_dailies(data: List | None):
        """Generates the plot of daily patient arrivals.

        Args:
        - data (List | None): Patient data from a dcc.Store object,
          after full preprocessing including adjustment of stay dates.
          See also `upload_details()` and `preprocessing.py`.

        Returns:
        - A plotly Figure object.
        """
        print('plot_dailies callback')
        if data is None:
            return go.Figure()  # empty figure

        daily_arrivals = get_daily_arrivals(pd.DataFrame(data))
        graph_daily_arrivals = px.bar(
            daily_arrivals, 'date', 'n_arrivals',
            labels=dict(date='Date', n_arrivals='Number of Arrivals')
        )
        return graph_daily_arrivals

    @app.callback(
        Output(GRAPHS.HOURLY_ARR, 'figure'),
        Input(STORES.ADJUSTED, 'data'),
        Input(INPUTS.DATE_RANGE, 'start_date'),
        Input(INPUTS.DATE_RANGE, 'end_date'),

        prevent_initial_call=True
    )
    def plot_hourlies(data: List | None, start_date: str, end_date: str):
        """Generates the hourly arrival distribution plot.

        Args:
        - data (List | None): Patient data from a dcc.Store object,
          after full preprocessing including adjustment of stay dates.
          See also `upload_details()` and `preprocessing.py`.
        - start_date (str): Start date for hourly distribution analysis, in ISO format.
        - end_date (str): End date for hourly distribution analysis, in ISO format.

        ISO format = 'YYYY-MM-DD'

        Returns:
        - A plotly Figure object.
        """
        print('plot_hourlies callback')

        if data is None or start_date is None or end_date is None or start_date > end_date:
            return go.Figure()  # empty figure

        hourly_arrivals = get_hourly_arrivals(
            pd.DataFrame(data), start_date, end_date)
        graph_hourly_arrivals = px.bar(
            hourly_arrivals, 'hour', 'freq',
            labels=dict(hour='Hour of Day', freq='Frequency')
        )
        graph_hourly_arrivals.update_layout(yaxis_tickformat='.2%')

        return graph_hourly_arrivals

    @app.callback(
        Output(TABLES.SURVIVAL, 'data'),
        Output(TABLES.SURVIVAL, 'columns'),
        Output(TABLES.ICU, 'data'),
        Output(TABLES.ICU, 'columns'),
        Output(NUMBER.PRE_ICU, 'children'),
        Input(STORES.ADJUSTED, 'data'),
        Input(INPUTS.DATE_RANGE, 'start_date'),
        Input(INPUTS.DATE_RANGE, 'end_date'),

        prevent_initial_call=True
    )
    def update_mortalities(data: List | None, start_date: str, end_date: str):
        """Update the Mortality tab when new patient data is uploaded.

        Args:
        - data (List | None): Patient data from a dcc.Store object,
          after full preprocessing including adjustment of stay dates.
          See also `upload_details()` and `preprocessing.py`.
        - start_date (str): Start date for hourly distribution analysis, in ISO format.
        - end_date (str): End date for hourly distribution analysis, in ISO format.

        ISO format = 'YYYY-MM-DD'

        Returns:
        1. Data for the survival rates table.
        2. Formatting information for the survival rates table.
        3. Data for the ICU admission rates table.
        4. Formatting information for the ICU admission rates table.
        5. HTML span object containing the percentage of ICU patients with a pre-ICU stay.
        """
        print('update_mortalities callback')

        if data is None or start_date is None or end_date is None or start_date > end_date:
            return None, None, None, None, None

        df_survival, df_icu_rate, pre_icu_rate \
            = get_mortality_info(pd.DataFrame(data), start_date, end_date)

        return (
            df_survival.to_dict('records'), SURVIVAL_TABLE_FORMATTING,
            df_icu_rate.to_dict('records'), ICU_RATE_TABLE_FORMATTING,
            f'{pre_icu_rate:.2%}'
        )

    @app.callback(
        Output(TABLES.LOS, 'data'),
        Output(TABLES.LOS, 'columns'),
        Output(GRAPHS.LOS, 'figure'),
        Input(STORES.ADJUSTED, 'data'),
        Input(INPUTS.DATE_RANGE, 'start_date'),
        Input(INPUTS.DATE_RANGE, 'end_date'),
        Input(INPUTS.MAX_GIM, 'value'),
        Input(INPUTS.MAX_ICU, 'value'),
        Input(INPUTS.MAX_POST_ICU, 'value'),

        prevent_initial_call=True
    )
    def update_los(
            data: List | None,
            start_date: str,
            end_date: str,
            max_gim: int | float | None,
            max_icu: int | float | None,
            max_post_icu: int | float | None
    ):
        """Update the LoS tab when new patient data is uploaded.

        Args:
        - data (List | None): Patient data from a dcc.Store object,
          after full preprocessing including adjustment of stay dates.
          See also `upload_details()` and `preprocessing.py`.
        - start_date (str): Start date for hourly distribution analysis, in ISO format.
        - end_date (str): End date for hourly distribution analysis, in ISO format.
        - max_gim (int | float | None): Cutoff threshold for outliers, GIM LoS
        - max_icu (int | float | None): Cutoff threshold for outliers, ICU LoS
        - max_post_icu (int | float | None): Cutoff threshold for outliers, post-ICU LoS

        ISO format = 'YYYY-MM-DD'.

        Outliers (unusually long stays) are ignored when fitting LoS distributions.

        Returns:
        1. Data for the LoS table, containing fitted LoS distributions and other information.
        2. Formatting information for the LoS table.
        3. Box plots for the LoS information, as a plotly Figure.
        """
        print('update_los callback')

        if data is None or start_date is None or end_date is None or start_date > end_date:
            return None, None, None

        df_los = get_los_info(
            pd.DataFrame(
                data), start_date, end_date, max_gim, max_icu, max_post_icu
        )
        graph_los = plot_los_info(pd.DataFrame(data), start_date, end_date)

        return df_los.to_dict('records'), LOS_TABLE_FORMATTING, graph_los

    @app.callback(
        Output(DOWNLOAD.BUTTON, 'disabled'),
        Output(DOWNLOAD.BUTTON, 'color'),
        Input(STORES.ADJUSTED, 'data'),
        prevent_initial_call=True
    )
    def toggle_download_button(data: List | None):
        """Enable the download button if valid patient data has been uploaded.
        Also set the colour of the button to green or grey based on the button status."""
        if data is None:
            return True, 'secondary'
        return False, 'success'

    @app.callback(
        Output(STATUS.START_DATE, 'children'),
        Input(INPUTS.DATE_RANGE, 'start_date'),
        prevent_initial_call=True
    )
    def show_start_date(start_date: str):
        """Update the start date in the config summary in the Download tab
        whenever the DatePicker is modified.
        The selected date range affects which patients are used for arrival
        distribution, mortality, and LoS analysis."""
        return start_date

    @app.callback(
        Output(STATUS.END_DATE, 'children'),
        Input(INPUTS.DATE_RANGE, 'end_date'),
        prevent_initial_call=True
    )
    def show_end_date(end_date: str):
        """Update the end date in the config summary in the Download tab
        whenever the DatePicker is modified.
        The selected date range affects which patients are used for arrival
        distribution, mortality, and LoS analysis."""
        return end_date

    @app.callback(
        Output(STATUS.MAX_GIM, 'children'),
        Input(INPUTS.MAX_GIM, 'value'),
        prevent_initial_call=True
    )
    def show_max_gim(max_gim: int | float | None):
        """Update the max GIM cutoff for LoS analysis in the config summary
        in the Download tab when the corresponding input field in the LoS tab is modified."""
        return '(Unlimited)' if max_gim is None else str(max_gim)

    @app.callback(
        Output(STATUS.MAX_ICU, 'children'),
        Input(INPUTS.MAX_ICU, 'value'),
        prevent_initial_call=True
    )
    def show_max_icu(max_icu: int | float | None):
        """Update the max ICU cutoff for LoS analysis in the config summary
        in the Download tab when the corresponding input field in the LoS tab is modified."""
        return '(Unlimited)' if max_icu is None else str(max_icu)

    @app.callback(
        Output(STATUS.MAX_POST_ICU, 'children'),
        Input(INPUTS.MAX_POST_ICU, 'value'),
        prevent_initial_call=True
    )
    def show_max_post_icu(max_post_icu: int | float | None):
        """Update the max post-ICU cutoff for LoS analysis in the config summary
        in the Download tab when the corresponding input field in the LoS tab is modified."""
        return '(Unlimited)' if max_post_icu is None else str(max_post_icu)

    @app.callback(
        Output(DOWNLOAD.CONTENTS, 'data'),
        Input(DOWNLOAD.BUTTON, 'n_clicks'),
        State(STORES.ADJUSTED, 'data'),
        State(INPUTS.DATE_RANGE, 'start_date'),
        State(INPUTS.DATE_RANGE, 'end_date'),
        State(INPUTS.MAX_GIM, 'value'),
        State(INPUTS.MAX_ICU, 'value'),
        State(INPUTS.MAX_POST_ICU, 'value'),

        prevent_initial_call=True
    )
    def download_config(
            _,
            data: list | None,
            start_date: str,
            end_date: str,
            max_gim: int | float | None,
            max_icu: int | float | None,
            max_post_icu: int | float | None
    ):
        """Generate a config file for download upon clicking the matching
        dash Download button.

        Args:
        - _ (int): Number of times the button has been clicked.
        - data (list | None): Patient data from a dcc.Store object,
          after full preprocessing including adjustment of stay dates.
          See also `upload_details()` and `preprocessing.py`.
        - start_date (str): Start date for hourly distribution analysis, in ISO format.
        - end_date (str): End date for hourly distribution analysis, in ISO format.
        - max_gim (int | float | None): Cutoff threshold for outliers, GIM LoS
        - max_icu (int | float | None): Cutoff threshold for outliers, ICU LoS
        - max_post_icu (int | float | None): Cutoff threshold for outliers, post-ICU LoS

        ISO format = 'YYYY-MM-DD'.

        Outliers (unusually long stays) are ignored when fitting LoS distributions.

        Returns:
        - An .xlsx file, encoded for dash's Download control.
        """
        if data is None:
            return dash.no_update
        df = pd.DataFrame(data)
        return gen_config_file(df, start_date, end_date, max_gim, max_icu, max_post_icu)
