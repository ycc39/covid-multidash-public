"""Functions for computing/displaying length-of-stay data."""

from collections import namedtuple
from datetime import timedelta

import pandas as pd
import plotly.graph_objects as go
import reliability as rel

DummyLoS = namedtuple('DummyLoS', 'param_title_long mean')
DUMMY_LOS = DummyLoS('', float('nan'))


def get_los_info(
        df: pd.DataFrame,
        start_date: str,
        end_date: str,
        max_gim: int | float | None,
        max_icu: int | float | None,
        max_post_icu: int | float | None
):
    """Generate a dataframe of LoS information.

    Args:
    - data (List | None): Adjusted patient stay data, from a dcc.Store object.
    - start_date (str): Start date for hourly distribution analysis, in ISO format.
    - end_date (str): End date for hourly distribution analysis, in ISO format.
    - max_gim (int | float | None): Cutoff threshold for outliers, GIM LoS
    - max_icu (int | float | None): Cutoff threshold for outliers, ICU LoS
    - max_post_icu (int | float | None): Cutoff threshold for outliers, post-ICU LoS

    ISO format = 'YYYY-MM-DD'.

    Outliers (unusually long stays) are ignored when fitting LoS distributions.

    Returns:
    - A pandas DataFrame.
    """
    admissions = pd.to_datetime(df.Admission)

    # half-open interval: [start_date midnight, end_date+1 midnight)
    start = pd.to_datetime(start_date)  #
    end = pd.to_datetime(end_date) + timedelta(days=1)

    # Get the patients admitted within the set dates
    df2 = df.loc[(admissions >= start) & (admissions < end)]
    df2.assign(Admission=pd.to_datetime(df2.Admission))
    df2.assign(ICU_Admission=pd.to_datetime(df2.ICU_Admission))

    if max_gim is None:
        max_gim = float('inf')
    if max_icu is None:
        max_icu = float('inf')
    if max_post_icu is None:
        max_post_icu = float('inf')

    gim_patients = df2.loc[pd.isna(df2.ICU_Admission) & (df2.LoS < max_gim)]
    pre_icu_patients = df2.loc[~pd.isna(
        df2.ICU_Admission) & (df2.LoS_preICU > 0)]
    icu_patients = df2.loc[~pd.isna(
        df2.ICU_Admission) & (df2.LoS_ICU < max_icu)]
    post_icu_patients = df2.loc[
        ~pd.isna(df2.ICU_Admission)
        & df2.Survived
        & ~df2.Censored_ICU
        & (df2.LoS_postICU > 0)
        & (df2.LoS_postICU < max_post_icu)
    ]

    gim_patients_survived = gim_patients.loc[gim_patients.Survived]
    gim_patients_died = gim_patients.loc[~gim_patients.Survived]

    # Fit distribution for GIM-only stays (survived patients)
    try:
        los_gim_survived = rel.Fitters.Fit_Weibull_3P(
            gim_patients_survived.loc[~gim_patients_survived.Censored, 'LoS'].to_list(),
            gim_patients_survived.loc[gim_patients_survived.Censored, 'LoS'].to_list(),
            show_probability_plot=False,
            print_results=False
        ).distribution
    except:
        los_gim_survived = DUMMY_LOS

    # Fit distribution for GIM-only stays (deceased patients)
    try:
        los_gim_died = rel.Fitters.Fit_Weibull_3P(
            gim_patients_died.LoS.to_list(),  # By definition, dead patient LoSs are uncensored
            show_probability_plot=False,
            print_results=False
        ).distribution
    except:
        los_gim_died = DUMMY_LOS

    # Fit distribution for pre-ICU stays
    try:
        los_pre_icu = rel.Fitters.Fit_Weibull_3P(
            # By definition, preICU LoSs are uncensored
            pre_icu_patients.LoS_preICU.to_list(),
            show_probability_plot=False,
            print_results=False
        ).distribution
    except:
        los_pre_icu = DUMMY_LOS

    # Fit distribution for ICU stays
    try:
        los_icu = rel.Fitters.Fit_Weibull_3P(
            icu_patients.loc[~icu_patients.Censored_ICU, 'LoS_ICU'].to_list(),
            icu_patients.loc[icu_patients.Censored_ICU, 'LoS_ICU'].to_list(),
            show_probability_plot=False,
            print_results=False
        ).distribution
    except:
        los_icu = DUMMY_LOS

    # Fit distribution for post-ICU stays
    try:
        # For post-ICU stays, only count patients that have survived and exited the ICU,
        # and discard zeros
        los_post_icu = rel.Fitters.Fit_Weibull_3P(
            post_icu_patients
            .loc[~post_icu_patients.Censored_postICU, 'LoS_postICU'].to_list(),
            post_icu_patients
            .loc[post_icu_patients.Censored_postICU, 'LoS_postICU'].to_list(),
            show_probability_plot=False,
            print_results=False
        ).distribution
    except:
        los_post_icu = DUMMY_LOS

    ret = pd.DataFrame(dict(
        LoS_type=[
            'GIM-only (survived)', 'GIM-only (died)',
            'pre-ICU', 'ICU', 'post-ICU',
        ],
        fitted_dist=[
            los_gim_survived.param_title_long,
            los_gim_died.param_title_long,
            los_pre_icu.param_title_long,
            los_icu.param_title_long,
            los_post_icu.param_title_long
        ],
        n_uncensored=[
            len(gim_patients_survived.loc[~gim_patients_survived.Censored]),
            len(gim_patients_died),
            len(pre_icu_patients),
            len(icu_patients.loc[~icu_patients.Censored_ICU]),
            len(post_icu_patients.loc[~post_icu_patients.Censored_postICU])
        ],
        n=[
            len(gim_patients_survived),
            len(gim_patients_died),
            len(pre_icu_patients),
            len(icu_patients),
            len(post_icu_patients)
        ],
        dist_mean=[
            los_gim_survived.mean,
            los_gim_died.mean,
            los_pre_icu.mean,
            los_icu.mean,
            los_post_icu.mean
        ],
        data_mean=[
            gim_patients_survived.LoS.mean(),
            gim_patients_died.LoS.mean(),
            pre_icu_patients.LoS_preICU.mean(),
            icu_patients.LoS_ICU.mean(),
            post_icu_patients.LoS_postICU.mean()
        ]
    ))

    return ret


def plot_los_info(df: pd.DataFrame, start_date: str, end_date: str):
    """Plot LoS information as a plotly Figure.

    Args:
    - data (List | None): Adjusted patient stay data, from a dcc.Store object.
    - start_date (str): Start date for hourly distribution analysis, in ISO format.
    - end_date (str): End date for hourly distribution analysis, in ISO format.

    ISO format = 'YYYY-MM-DD'.

    Returns:
    - A plotly Figure.
    """
    admissions = pd.to_datetime(df.Admission)

    # half-open interval: [start_date midnight, end_date+1 midnight)
    start = pd.to_datetime(start_date)  #
    end = pd.to_datetime(end_date) + timedelta(days=1)

    # Get a list of patients admitted within the set dates
    df2 = df.loc[(admissions >= start) & (admissions < end)]
    df2.assign(Admission=pd.to_datetime(df2.Admission))
    df2.assign(ICU_Admission=pd.to_datetime(df2.ICU_Admission))

    gim_patients = df2.loc[pd.isna(df2.ICU_Admission)][[
        'Survived', 'LoS', 'Censored']]
    icu_patients = df2.loc[~pd.isna(df2.ICU_Admission)][
        ['Survived', 'LoS', 'Censored', 'LoS_preICU', 'LoS_ICU',
            'Censored_ICU', 'LoS_postICU', 'Censored_postICU']
    ]

    # Split the patient list by type
    gim_patients_survived = gim_patients.loc[gim_patients.Survived]
    gim_patients_died = gim_patients.loc[~gim_patients.Survived]
    patients_post_icu = icu_patients.loc[
        icu_patients.Survived
        & ~icu_patients.Censored_ICU
        & (icu_patients.LoS_postICU > 0)
    ]

    fig = go.Figure()

    # plots added first appear on the bottom by default
    fig.add_trace(go.Box(
        x=patients_post_icu.LoS_postICU,
        name='post-ICU',
        xhoverformat='.2f',
        hoverinfo='x',
        hoverlabel=dict(namelength=0)
    ))
    fig.add_trace(go.Box(
        x=icu_patients.LoS_ICU,
        name='ICU',
        xhoverformat='.2f',
        hoverinfo='x',
        hoverlabel=dict(namelength=0)
    ))
    fig.add_trace(go.Box(
        x=icu_patients.loc[icu_patients.LoS_preICU > 0, 'LoS_preICU'],
        name='pre-ICU',
        xhoverformat='.2f',
        hoverinfo='x',
        hoverlabel=dict(namelength=0)
    ))
    fig.add_trace(go.Box(
        x=gim_patients_died.LoS,
        name='GIM-only (died)',
        xhoverformat='.2f',
        hoverinfo='x',
        hoverlabel=dict(namelength=0)
    ))
    fig.add_trace(go.Box(
        x=gim_patients_survived.LoS,
        name='GIM-only (survived)',
        xhoverformat='.2f',
        hoverinfo='x',
        hoverlabel=dict(namelength=0)
    ))

    # fig.update_xaxes(type='log')
    fig.update_xaxes(title_text='Length of Stay (days)')
    fig.update_layout(showlegend=False)
    return fig
