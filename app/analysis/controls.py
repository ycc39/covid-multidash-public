"""Define names for the dash and HTML controls."""
from collections import namedtuple

TOP = namedtuple("TOP", ["BUTTON_HOME", "UPLOAD"])(
    "button_home", "upload_details"
)
"""Defines controls in the top Div."""

STORES = namedtuple("STORES", ["RAW", "CLEANED", "CURRENT", "ADJUSTED", "ADJUSTED_CURRENT"],)(
    "store-raw-upload",
    "store-cleaned",
    "store-current",
    "store-adjusted",
    "store-adjusted-current"
)
"""Defines dash.dcc.Store objects."""

STATUS = namedtuple(
    "STATUS",
    ["UPLOAD", "START_DATE", "END_DATE", "MAX_GIM", "MAX_ICU", "MAX_POST_ICU"]
)(
    "div-upload-status",
    "span-start-date",
    "span-end-date",
    "span-max-gim",
    "span-max-icu",
    "span-max-posticu"
)
"""Defines HTML components for showing current configuration values."""

INPUTS = namedtuple(
    "INPUTS",
    ["DATE_RANGE", "VIEW_OPTIONS", "VIEW_OPTIONS_ADJ",
        "MAX_GIM", "MAX_ICU", "MAX_POST_ICU"],
)(
    "date-picker-range",
    "view-options",
    "view-options-adjusted",
    "input-max-gim",
    "input-max-icu",
    "input-max-posticu"
)
"""Defines HTML components for user input."""

TABLES = namedtuple("TABLES", ["DETAILS", "ADJUSTED", "SURVIVAL", "ICU", "LOS"])(
    "table-details",
    "table-adjusted",
    "table-survival-rates",
    "table-icu-rates",
    "table-los"
)
"""Defines dash Table components."""

NUMBER = namedtuple("NUMBER", ["PRE_ICU"])('span-pre-icu-rate')
"""Defines an HTML span for showing a single numeric value."""

GRAPHS = namedtuple("GRAPHS", ["DAILY_ARR", "HOURLY_ARR", "LOS"])(
    "graph-daily-arrivals",
    "graph-hourly-arrivals",
    "graph-los"
)
"""Defines Plotly graph objects."""

DOWNLOAD = namedtuple("DOWNLOAD", ["BUTTON", "CONTENTS"])(
    'download-button', 'download-contents'
)
"""Defines controls for downloading the final config file."""
