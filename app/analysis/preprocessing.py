"""Preprocessing functions for patient stay data."""

from datetime import timedelta

import numpy as np
import pandas as pd

from .cols import COLS

STAY_THRESHOLD = 0.3
DELTA_THRESHOLD = 15


def check_cols(df: pd.DataFrame):
    """Check that imported patient data contains all the required columns."""
    return all(k in df.columns.values for k in COLS)


def data_clean(df: pd.DataFrame):
    """Initial stage of data pre-processing."""

    # Drop non-admitted patients
    df2 = df.dropna(subset=['Admission'])
    df2 = df2.sort_values(by=['Admission'])
    df2.reset_index(drop=True, inplace=True)

    # Drop patients with non-Covid initial stays
    df2 = df2.loc[(df2.Discharge > df2.First_Pos_Collected)
                  | df2.Discharge.isna()]

    # Drop fractional seconds in datetime columns
    dt_cols = list(df2.select_dtypes(np.datetime64).columns)
    for col in dt_cols:
        # Note that "df.assign(col = ...)" creates a column called 'col' instead
        # So we need to manually create a dict for kwargs
        df2 = df2.assign(**{col: df2[col].dt.floor('S')})

    # Delete timestamps after 'now'
    df2.loc[df2.Discharge > df2.now, 'Discharge'] = pd.NaT
    df2.loc[df2.ICU_Discharge > df2.now, 'ICU_Discharge'] = pd.NaT

    # Remove ICU stays before first positive test
    df2.loc[
        df2.ICU_Discharge < df2.First_Pos_Collected,
        ['ICU_Admission', 'ICU_Discharge']
    ] = pd.NaT

    # Mark hospital-acquired infections
    df2 = df2.assign(Hospital=df2.Acquisition.str.contains('Hospital'))
    df2.drop('Acquisition', axis='columns', inplace=True)

    df2 = df2.assign(Survived=~df2.Summary.str.contains('Dead'))
    df2.drop('Summary', axis='columns', inplace=True)

    # Rearrange columns
    first_cols = ['Age', 'Survived']
    df2 = df2[first_cols + [c for c in df2.columns if c not in first_cols]]

    return df2


def current_patients(df: pd.DataFrame):
    """Given a dataframe of patient data, keep only the patients still in GIM or ICU."""

    # Handle current GIM patients (first stay)
    df2 = df.assign(Censored=df.Discharge.isna())

    # Add current GIM patients (readmission stay)
    censored2 = (df2.ReAdmissionDisch.isna()) & ~(df2.ReAdmission.isna())
    df2 = df2.assign(Censored=df2.Censored | censored2)

    # Handle current ICU patients
    censored_icu = (df2.ICU_Discharge.isna()) & ~(df2.ICU_Admission.isna())
    df2 = df2.assign(Censored_ICU=censored_icu)

    df2 = df2.loc[df2.Censored | df2.Censored_ICU]  # filter rows
    df2.drop('Censored', axis='columns', inplace=True)
    df2.drop('Censored_ICU', axis='columns', inplace=True)

    return df2


def fill_missing_discharges(df: pd.DataFrame):
    """Substitute "now" column value for the discharge datetimes of current patients.
    See internal comments for preprocessing steps.
    """

    # Handle current patients (first stay)
    df = df.assign(Censored=df.Discharge.isna())
    df.loc[df.Censored, 'Discharge'] = df.loc[df.Censored, 'now']

    # Handle current patients (readmission stay)
    censored2 = (df.ReAdmissionDisch.isna()) & ~(df.ReAdmission.isna())
    df = df.assign(Censored=df.Censored | censored2)
    df.loc[censored2, 'ReAdmissionDisch'] = df.loc[censored2, 'now']

    # Handle current ICU patients
    censored_icu = (df.ICU_Discharge.isna()) & ~(df.ICU_Admission.isna())
    censored_post_icu = df.Censored & ~(df.ICU_Discharge.isna())
    df = df.assign(Censored_ICU=censored_icu,
                   Censored_postICU=censored_post_icu)

    # Set 'Discharge' of current ICU patients
    df.loc[censored_icu, 'ICU_Discharge'] = df.loc[censored_icu, 'Discharge']
    df.loc[censored_icu & df.Censored,
           'ICU_Discharge'] = df.loc[censored_icu, 'now']

    return df


def adjust_dates(df: pd.DataFrame):
    """Adjust patient stay dates for hospital-acquired infections and readmitted patients.
    See internal comments for preprocessing steps."""

    # Admission = First_Pos_Collected for hospital-acquired infections
    df2 = df.assign(Admission=df.Admission)
    df2.loc[df2.Hospital, 'Admission'] = df2.First_Pos_Collected
    df2.loc[df2.ICU_Admission < df2.Admission, 'ICU_Admission'] = df2.Admission

    # isReAdmitted flag
    df2 = df2.assign(isReAdmitted=True)
    df2.loc[df2.ReAdmission.isna(), 'isReAdmitted'] = False

    # LoS
    day = timedelta(days=1)
    df2 = df2.assign(LoS_First=(df2.Discharge - df2.Admission) / day)
    df2 = df2.assign(LoS_ReAdmission=(
        df2.ReAdmissionDisch - df2.ReAdmission) / day)
    df2 = df2.assign(ReAdmission_Delta=(
        df2.ReAdmission - df2.First_Pos_Collected) / day)
    df2 = df2.assign(LoS_ICU=(df2.ICU_Discharge - df2.ICU_Admission) / day)

    # Revised Stays
    for i in df2.index.to_list():
        if df2.isReAdmitted[i] and df2.ReAdmission_Delta[i] < DELTA_THRESHOLD:
            # Patient readmitted within time limit
            if df2.LoS_First[i] < STAY_THRESHOLD <= df2.LoS_ReAdmission[i]:
                # First stay is short: Keep readmission stay only
                df2.loc[i, 'Admission'] = df2.loc[i, 'ReAdmission']
                df2.loc[i, 'Discharge'] = df2.loc[i, 'ReAdmissionDisch']
            elif df2.LoS_First[i] >= STAY_THRESHOLD and df2.LoS_ReAdmission[i] >= STAY_THRESHOLD:
                # Both stays are long: Shift readmission stay forward in time to close gap
                df2.loc[i, 'Discharge'] = df2.loc[i, 'Discharge'] \
                    + timedelta(days=df2.LoS_ReAdmission[i])
        elif df2.isReAdmitted[i] and df2.ReAdmission_Delta[i] >= DELTA_THRESHOLD:
            # Patient readmitted beyond time limit: Keep initial stay only and unset censor flags
            df2.loc[i, 'Censored'] = False

    df2 = df2.assign(LoS=(df2.Discharge - df2.Admission) / day)
    df2 = df2.assign(LoS_preICU=(df2.ICU_Admission - df2.Admission) / day)
    df2 = df2.assign(LoS_postICU=(df2.Discharge - df2.ICU_Discharge) / day)

    df2.drop(
        ['ReAdmission', 'ReAdmissionDisch', 'First_Pos_Collected',
         'now', 'Hospital', 'isReAdmitted'],
        axis=1, inplace=True
    )

    # Drop patients with LoS < STAY_THRESHOLD
    df2 = df2.loc[df2.LoS >= STAY_THRESHOLD]

    # Sort patients by revised admission date
    df2 = df2.sort_values(by=['Admission'])
    df2.reset_index(drop=True, inplace=True)

    return df2
