"""Generate a dataframe of daily arrivals."""

import pandas as pd


def get_daily_arrivals(df: pd.DataFrame):
    """Generate a dataframe of daily arrivals.

    Args:
        df (pd.DataFrame): Patient data from a dcc.Store object,
          after full preprocessing including adjustment of stay dates.
          See also `controls.upload_details()` and `preprocessing.py`.

    Returns:
    - Pandas DataFrame of daily arrivals.
    """
    admissions = pd.to_datetime(df.Admission)
    admission_dates = admissions.dt.floor(freq='D')  # Admission date for each patient
    min_day = min(admission_dates)
    max_day = max(admission_dates)
    date_range = pd.date_range(min_day, max_day).to_list()

    # Count the number of admissions for each date in date_range
    n_arrivals = [
        len([t for t in admission_dates if t == x])
        for x in date_range
    ]

    return pd.DataFrame(
        dict(date=date_range, n_arrivals=n_arrivals)
    )
