"""Generate a dataframe containing the hourly arrival distribution."""

from datetime import timedelta
from sys import float_info

import numpy as np
import pandas as pd


def get_hourly_arrivals(df: pd.DataFrame, start_date: str, end_date: str):
    """Generate a dataframe containing the hourly arrival distribution.

    Args:
    - df (pd.DataFrame): Adjusted patient stay data.
    - start_date (str): Start date for hourly distribution analysis, in ISO format.
    - end_date (str): End date for hourly distribution analysis, in ISO format.

    ISO format = 'YYYY-MM-DD'

    Returns:
    - A pandas DataFrame.
    """
    admissions = pd.to_datetime(df.Admission)

    # half-open interval: [start_date midnight, end_date+1 midnight)
    start = pd.to_datetime(start_date)
    end = pd.to_datetime(end_date) + timedelta(days=1)
    admissions = admissions.loc[(admissions >= start) & (admissions < end)]

    hours = admissions.dt.hour
    hour_counts = [
        len([h for h in hours if h == x])
        for x in list(range(24))
    ]

    return pd.DataFrame(
        dict(
            hour=list(range(24)),
            freq=np.array(hour_counts) / (np.sum(hour_counts) + float_info.min)
        )
    )
