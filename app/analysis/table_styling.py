"""Formatting and styling for dash Tables."""

from typing import Dict, List

from dash.dash_table.Format import Format, Scheme


def red_bold_table_style(query: str, col_id: str):
    """Returns a dict for conditional formatting in a dash Table.

    Args:
    - query (str): Condition to match.
    - col_id (str): Column for condition checking.

    Returns:
    - A dict.
    """
    return {
        'if': dict(filter_query=query, column_id=col_id),
        'color': 'tomato',
        'fontWeight': 'bold'
    }


CURRENT_PATIENTS_TABLE_STYLE: List[Dict] = \
    [
        red_bold_table_style(
            '{Discharge} is blank && {Admission} < {now}', 'Admission'),
        red_bold_table_style(
            '{ICU_Discharge} is blank && {ICU_Admission} < {now}', 'ICU_Admission'
        ),
        red_bold_table_style(
            '{ReAdmissionDisch} is blank && {ReAdmission} < {now}', 'ReAdmission')
]
"""Style definitions for current patients. Highlights relevant cells in red."""

DAYS_FORMAT = Format(precision=3, scheme=Scheme.fixed)
PERCENTAGE_FORMAT = Format(precision=2, scheme=Scheme.percentage)

ADJUSTED_TABLE_FORMATTING: List[Dict] = [
    dict(id='Age', name='Age'),
    dict(id='Survived', name='Survived'),
    dict(id='Admission', name='Admission'),
    dict(id='Discharge', name='Discharge'),
    dict(id='ICU_Admission', name='ICU_Admission'),
    dict(id='ICU_Discharge', name='ICU_Discharge'),
    dict(id='Censored', name='Censored'),
    dict(id='Censored_ICU', name='Censored_ICU'),
    dict(id='Censored_postICU', name='Censored_postICU'),
    dict(id='LoS', name='LoS', type='numeric', format=DAYS_FORMAT),
    dict(id='LoS_preICU', name='preICU', type='numeric', format=DAYS_FORMAT),
    dict(id='LoS_ICU', name='ICU', type='numeric', format=DAYS_FORMAT),
    dict(id='LoS_postICU', name='postICU', type='numeric', format=DAYS_FORMAT)
]

# Note: to apply this style, the boolean columns marked here must be converted to strings first
ADJUSTED_TABLE_STYLE: List[Dict] = \
    [
        red_bold_table_style('{Censored} eq "true"', 'Censored'),
        red_bold_table_style('{Censored_ICU} eq "true"', 'Censored_ICU'),
        red_bold_table_style(
            '{Censored_postICU} eq "true"', 'Censored_postICU')
]
"""Style definitions to highlight censored LoS data."""

SURVIVAL_TABLE_FORMATTING: List[Dict] = [
    dict(id='AgeGroup', name='Age Group'),
    dict(id='survivedGIM', name='GIM only',
         type='numeric', format=PERCENTAGE_FORMAT),
    dict(id='survivedICU_Indirect', name='ICU, Indirect',
         type='numeric', format=PERCENTAGE_FORMAT),
    dict(id='survivedICU_Direct', name='ICU, Direct',
         type='numeric', format=PERCENTAGE_FORMAT)
]
"""Formatting definitions for survival rate table. Formats survival rates as percentages."""

ICU_RATE_TABLE_FORMATTING: List[Dict] = [
    dict(id='AgeGroup', name='Age Group'),
    dict(id='is_icu', name='ICU Admission Rate',
         type='numeric', format=PERCENTAGE_FORMAT)
]
"""Formatting definitions for ICU admission rate table. Formats admission rates as percentages."""

LOS_TABLE_FORMATTING: List[Dict] = [
    dict(id='LoS_type', name='LoS Type'),
    dict(id='fitted_dist', name='Fitted Distribution'),
    dict(id='n_uncensored', name='# (Completed)'),
    dict(id='n', name='# (Total)'),
    dict(id='dist_mean', name='Distribution Mean',
         type='numeric', format=DAYS_FORMAT),
    dict(id='data_mean', name='Data Mean', type='numeric', format=DAYS_FORMAT)
]
"""Formatting definitions for LoS table.  Controls number of decimal places shown for LoS."""
