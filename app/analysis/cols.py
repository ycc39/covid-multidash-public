"""Define mandatory columns in the uploaded patient data file."""

COLS = [
    'Age', 'Summary', 'Admission', 'Discharge', 'ICU_Admission', 'ICU_Discharge',
    'ReAdmission', 'ReAdmissionDisch', 'First_Pos_Collected', 'Acquisition', 'now'
]
"""List of mandatory columns in the uploaded patient data file."""
