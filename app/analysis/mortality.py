"""Functions for computing mortality info."""
from datetime import timedelta

import pandas as pd


def get_mortality_info(df: pd.DataFrame, start_date: str, end_date: str):
    """Generate LoS information.

    Args:
    - data (List | None): Adjusted patient stay data, from a dcc.Store object.
    - start_date (str): Start date for hourly distribution analysis, in ISO format.
    - end_date (str): End date for hourly distribution analysis, in ISO format.

    ISO format = 'YYYY-MM-DD'.

    Returns:
    - Pandas DataFrames for mortality and ICU admission rates, and the proportion
    of ICU patients with non-zero pre-ICU stays (float).
    """
    admissions = pd.to_datetime(df.Admission)

    # half-open interval: [start_date midnight, end_date+1 midnight)
    start = pd.to_datetime(start_date)  #
    end = pd.to_datetime(end_date) + timedelta(days=1)

    # Get the patients admitted within the set dates
    df2 = df.loc[(admissions >= start) & (admissions < end)]
    df2.assign(Admission=pd.to_datetime(df2.Admission))
    df2.assign(ICU_Admission=pd.to_datetime(df2.ICU_Admission))

    # Assign age groups
    df2 = df2.assign(AgeGroup='50+')
    df2.loc[df2.Age < 50, 'AgeGroup'] = '16-50'
    df2.loc[df2.Age < 16, 'AgeGroup'] = 'U16'

    # Survival Rate: GIM only
    survived_gim \
        = df2.loc[pd.isna(df2.ICU_Admission), ['AgeGroup', 'Survived']] \
        .groupby('AgeGroup').mean().reindex(['U16', '16-50', '50+'])
    survived_gim.loc['All', 'Survived'] = \
        df2.loc[pd.isna(df2.ICU_Admission), 'Survived'].mean()

    # Survival Rate: ICU, from GIM
    survived_icu_indirect \
        = df2.loc[df2.ICU_Admission > df2.Admission, ['AgeGroup', 'Survived']] \
        .groupby('AgeGroup').mean().reindex(['U16', '16-50', '50+'])
    survived_icu_indirect.loc['All', 'Survived'] = \
        df2.loc[df2.ICU_Admission > df2.Admission, 'Survived'].mean()

    # Survival Rate: ICU, direct
    survived_icu_direct \
        = df2.loc[df2.ICU_Admission == df2.Admission, ['AgeGroup', 'Survived']] \
        .groupby('AgeGroup').mean().reindex(['U16', '16-50', '50+'])
    survived_icu_direct.loc['All', 'Survived'] = \
        df2.loc[df2.ICU_Admission == df2.Admission, 'Survived'].mean()

    df_survival = pd.DataFrame({
        'survivedGIM': survived_gim.Survived,
        'survivedICU_Indirect': survived_icu_indirect.Survived,
        'survivedICU_Direct': survived_icu_direct.Survived,
    })

    # ICU Admission Rate (proportion of all patients that enter the ICU, direct or indirect)
    df_icu_rate \
        = df2.assign(is_icu=~pd.isnull(df2.ICU_Admission))[['AgeGroup', 'is_icu']] \
        .groupby('AgeGroup').mean().reindex(['U16', '16-50', '50+'])
    df_icu_rate.loc['All', 'is_icu'] = \
        (~pd.isnull(df2.ICU_Admission)).mean()

    # Proportion of ICU patients with non-zero pre-ICU LoS
    try:
        pre_icu_rate \
            = len(df2.loc[df2.ICU_Admission > df2.Admission, 'Survived']) \
            / len(df2.loc[~pd.isna(df2.ICU_Admission), 'Survived'])
    except ZeroDivisionError:
        pre_icu_rate = float('nan')

    return (
        df_survival.reset_index(),
        df_icu_rate.reset_index(),
        pre_icu_rate
    )
