"""Generate an .xlsx config file."""

from io import BytesIO
import pandas as pd
import openpyxl as xl
from openpyxl.worksheet.worksheet import Worksheet
from openpyxl.worksheet.dimensions import ColumnDimension, DimensionHolder
from openpyxl.styles.fonts import Font
from openpyxl.styles.numbers import FORMAT_PERCENTAGE_00, FORMAT_NUMBER, FORMAT_DATE_YYYYMMDD2
from dash import dcc

from .daily_arrivals import get_daily_arrivals
from .hourly_arrivals import get_hourly_arrivals
from .los import get_los_info
from .mortality import get_mortality_info


def gen_config_file(
        df: pd.DataFrame,
        start_date: str,
        end_date: str,
        max_gim: int | float | None,
        max_icu: int | float | None,
        max_post_icu: int | float | None
):
    """Generate the config file for download.

    Args:
    - df (pd.DataFrame): Patient data from a dcc.Store object,
      after full preprocessing including adjustment of stay dates.
      See also `controls.upload_details()` and `preprocessing.py`.
    - start_date (str): Start date for hourly distribution analysis, in ISO format.
    - end_date (str): End date for hourly distribution analysis, in ISO format.
    - max_gim (int | float | None): Cutoff threshold for outliers, GIM LoS
    - max_icu (int | float | None): Cutoff threshold for outliers, ICU LoS
    - max_post_icu (int | float | None): Cutoff threshold for outliers, post-ICU LoS

    ISO format = 'YYYY-MM-DD'.

    Outliers (unusually long stays) are ignored when fitting LoS distributions.

    Returns:
    - An .xlsx file, encoded for transmission using a dcc.Download object.
    """
    daily_arrivals = get_daily_arrivals(df)
    daily_arrivals.columns = ['Date', '# Arrivals']
    hourly_arrivals = get_hourly_arrivals(df, start_date, end_date)
    hourly_arrivals.columns = ['Hour', 'Frequency']

    survival_data, icu_rate, pre_icu_rate = get_mortality_info(df, start_date, end_date)
    survival_data = survival_data.loc[survival_data.AgeGroup == 'All']
    survival_data = survival_data.set_index('AgeGroup').transpose().reset_index()
    survival_data.columns = ['Stay Type', 'Survival Rate']
    survival_data.loc[:, 'Stay Type'] = ['GIM Only', 'ICU, indirect', 'ICU, direct']
    icu_rate = icu_rate.loc[icu_rate.AgeGroup == 'All', 'is_icu'].reset_index(drop=True)[0]

    los_data = get_los_info(df, start_date, end_date, max_gim, max_icu, max_post_icu)
    los_data = los_data[['LoS_type', 'fitted_dist']]
    los_data.columns = ['LoS Type', 'Fitted Distribution']

    wb = xl.Workbook(iso_dates=True)

    # FIRST sheet: patient parameters
    ws = wb.active
    if ws is None:
        ws = wb.create_sheet("Patient Parameters")
    else:
        ws.title = "Patient Parameters"

    # Patient Parameters: Survival rate
    ws.cell(1, 1, value='Survival Rates').font = Font(bold=True)
    write_df(survival_data, ws, 2, 1, None, FORMAT_PERCENTAGE_00)

    ws.cell(7, 1, value='ICU admission rate (all patients)').font = Font(bold=True)
    ws.cell(7, 2, value=icu_rate).number_format = FORMAT_PERCENTAGE_00

    ws.cell(8, 1, value='Pre-ICU rate (ICU patients)').font = Font(bold=True)
    ws.cell(8, 2, value=pre_icu_rate).number_format = FORMAT_PERCENTAGE_00

    ws.cell(1, 4, value='Fitted Length of Stay Distributions').font = Font(bold=True)
    write_df(los_data, ws, 2, 4)

    dim_holder = DimensionHolder(worksheet=ws)
    dim_holder['A'] = ColumnDimension(ws, 'A', width=27.5)
    dim_holder['B'] = ColumnDimension(ws, 'B', width=11.0)
    dim_holder['C'] = ColumnDimension(ws, 'C', width=5.0)
    dim_holder['D'] = ColumnDimension(ws, 'D', width=17.5)
    dim_holder['E'] = ColumnDimension(ws, 'E', width=44.0)

    ws.column_dimensions = dim_holder

    # SECOND sheet: patient arrivals
    ws = wb.create_sheet("Patient Arrivals")

    ws.cell(1, 1, value='Hourly Arrival Distribution').font = Font(bold=True)
    write_df(hourly_arrivals, ws, 2, 1, None, FORMAT_PERCENTAGE_00)

    ws.cell(1, 4, value='Daily Arrivals').font = Font(bold=True)
    write_df(daily_arrivals, ws, 2, 4, FORMAT_DATE_YYYYMMDD2, FORMAT_NUMBER)

    dim_holder2 = DimensionHolder(worksheet=ws)
    dim_holder2['A'] = ColumnDimension(ws, 'A', width=9.5)
    dim_holder2['B'] = ColumnDimension(ws, 'B', width=12.5)
    dim_holder2['C'] = ColumnDimension(ws, 'C', width=5.0)
    dim_holder2['D'] = ColumnDimension(ws, 'D', width=11.0)
    dim_holder2['E'] = ColumnDimension(ws, 'E', width=8.25)

    ws.column_dimensions = dim_holder2

    ret = BytesIO()
    wb.save(ret)
    return dcc.send_bytes(
        ret.getvalue(),
        filename='config.xlsx',
        type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    )


def write_df(
        df: pd.DataFrame,
        ws: Worksheet,
        start_row: int,
        start_col: int,
        format_str0: str | None = None,
        format_str: str | None = None
):
    """Write a Pandas dataframe to an openpyxl worksheet, at the specified location.

    Args:
    - df (pd.DataFrame): The dataframe to write.
    - ws (Worksheet): The worksheet to write to.
    - start_row (int): The starting row for writing the data to the worksheet.
    - start_col (int): The starting column for writing the data to the worksheet.
    - format0 (str | None, optional): A format string for the first
    column in the dataframe. Defaults to None.
    - format (str | None, optional): A format string for all columns in
    the dataframe except the first column. Defaults to None.
    """
    # For simplicity, we will always keep the column names but not the row indexes
    nrows = df.shape[0]
    ncols = df.shape[1]
    colnames = df.columns

    # column names
    for c in range(ncols):
        ws.cell(start_row, start_col + c, value=colnames[c]).font = Font(bold=True)
    start_row += 1

    for r in range(nrows):
        for c in range(ncols):
            _cell = ws.cell(start_row + r, start_col + c, value=df.iloc[r, c])
            if (c == 0) and (format_str0 is not None):
                _cell.number_format = format_str0
            if (c > 0) and (format_str is not None):
                _cell.number_format = format_str
