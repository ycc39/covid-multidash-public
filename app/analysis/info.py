"""Definitions for the Info tab in the Analysis dashboard."""
import dash_bootstrap_components as dbc
from dash import dash, dcc, html


def info(app: dash.Dash):
    """Populate the Info tab in the Analysis dashboard."""
    return dbc.Accordion(
        [
            dbc.AccordionItem(
                dcc.Markdown(
                    """\
This dashboard computes patient parameters and arrival
patterns, given a list of patient stays as an Excel spreadsheet, and returns an .xlsx file
containing these parameters. The output .xlsx file can be used as input to the next
dashboard in this app: the "**Scenario Creation Dashboard**"."""
                ),
                title='Overview'
            ),
            dbc.AccordionItem(
                [
                    dcc.Markdown(
                        """\
There are two types of hospital beds in our model: **general
inpatient medical (GIM)** beds and **intensive care unit (ICU)** beds.  Covid patients flow
through the model according to the figure below:
"""
                    ),
                    html.Img(src=app.get_asset_url(
                        'flowchart.png'), width='900px')
                ],
                title='Patient Flow Model'
            ),
            dbc.AccordionItem(
                dcc.Markdown(
                    """\
The input file must be an .xls or .xlsx file containing
a single table. The table must start at Cell A1 and contain (at least) the following columns:

- **Age** (numeric): The patient's age.
- **Summary** (text): used to denote patient status, e.g. "Dead", "Discharged", "Current ICU", or
"Current IP non-ICU".
- **Admission**/**Discharge** (datetime): admission and discharge datetimes of each patient. Current
patients will have an admission time but no discharge time (blank cell).
- **ICU_Admission**/**ICU_Discharge** (datetime): admission and discharge datetimes for the ICU.
GIM-only patients will have both fields blank, while current ICU patients will have an
`ICU_Admission` entry but no `ICU_Discharge` entry.
- **ReAdmission**/**ReAdmissionDisch** (datetime): admission and discharge datetimes for
readmissions. Patients with only a single stay will have both fields blank, while current
readmitted patients will have a `ReAdmission` entry but no `ReAdmissionDisch` entry.
- **First_Pos_Collected** (datetime): collection datetime of each patient's first positive
Covid test.
- **Acquisition** (text): How the patient acquired Covid &mdash; hospital-acquired infections will
be treated specially when computing lengths of stay.
- **now** (datetime): datetime at which the status of a patient was last updated in the
spreadsheet.
"""
                ),
                title='Input File'
            ),
            dbc.AccordionItem(
                dcc.Markdown(
                    """\
The "View Data" tab shows patient details for each patient. The
information shown is as described as in the Input File section above, except `Summary` is
replaced with a binary column marking whether the patient survived, and `Acquisition`
is replaced with a binary column marking whether the infection was hospital-acquired.
"""
                ),
                title='View Data tab'
            ),
            dbc.AccordionItem(
                dcc.Markdown(
                    """\
The "Adjusted Stay Dates" tab shows adjusted stay dates for each
patient.  Each patient's stay is adjusted according to the following rules:

1. Any discharge or ICU discharge dates after the `now` entry of a patient is discarded and
the patient is treated as a current GIM/ICU patient.
2. Any ICU stays completed before a patient's first positive Covid test are discarded.
3. The discharge datetime of any current GIM patients is set to those patients' `now` entry.
4. The discharge datetime **and** ICU discharge datetime of any current GIM patients is set to those
patients' `now` entry.
5. The stays of readmitted patients are adjusted as follows:
- If the readmission is more than 15.0 days after the initial discharge, the readmission stay
is discarded.  If the patient is marked as Current, their "current" status will be
removed.
- Else, if the initial stay was less than 0.3 days and the readmission stay greater than 0.3
days in duration, only the readmission stay will be kept.
- Else, if both stays were greater than 0.3 days in duration, the durations of both stays
are added together and the readmission stay is shifted forward in time to create a
single continuous stay.
- Otherwise, only the initial stay is kept and the readmission stay is discarded.  If the
patient is marked as Current, their "current" status will be removed.
6. All stays shorter than 0.3 days (total of GIM and ICU stay) are discarded.

**Censor flags**

The displayed table contains three censor flag columns denoting whether a patient's GIM stay,
(for GIM-only patients), ICU stay, and/or post-ICU stay (in the GIM) are censored, i.e. the
patient is still currently in the GIM or ICU.

**LoS columns**

A patient's total length of stay (LoS), in days, is shown in the `LoS` column.  For ICU patients,
the pre-ICU, ICU, and post-ICU LoSs are shown in their respective columns.
"""
                ),
                title='Adjusted Stay Dates tab'
            ),
            dbc.AccordionItem(
                dcc.Markdown(
                    """\
**Date range picker: **
Only patients with arrival dates within the chosen date range will be used for statistical
analysis in the tabs shown with ***italic*** label text.

**Daily Arrivals tab: **
This tab shows a plot of the total number of patient arrivals on each day, based on the
adjusted datetimes (see the "Adjusted Stay Dates tab" section).

**Arrivals by Hour of Day tab: **
This tab shows a plot of the patient arrival distribution (using the adjusted datetimes)
by hour of day.

**Mortality tab: **
This tab shows the mortality rate of patients based on type (GIM-only, indirect ICU, and
direct ICU), the percentage of patients that are ICU patients, and the percentage of ICU patients
that enter the ICU indirectly (i.e., via GIM).

**LoS tab: **
This tab shows the LoS distribution for various types of patient stays within the model.
GIM-only stays are divided between those for survived patients and those for deceased patients,
while stays including the ICU are divided into three stages: pre-ICU, ICU, and post-ICU.

Filter options are provided to prune outliers with unusually long LoSs.  These filter settings
will also be applied to the final generated configuration (.xlsx) file.
"""
                ),
                title='Statistics tabs'
            ),
            dbc.AccordionItem(
                dcc.Markdown(
                    """\
The generated configuration file contains information necessary for
the other two stages of the Covid app, contained in a .xlsx file. Users can review the parameter
settings chosen before downloading the file.
"""
                ),
                title='Generating the config file'
            )
        ],
        style={'width': '1000px'}
    )
