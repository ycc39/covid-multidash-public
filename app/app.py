"""Entry point for the Flask app.
"""

import dash_bootstrap_components as dbc
import flask
from .analysis import dash_app as analysis_app
from .scenario import dash_app as scenario_app
from .sim import dash_app as sim_app
from werkzeug.middleware.dispatcher import DispatcherMiddleware as Dispatcher
from werkzeug.serving import run_simple

server = flask.Flask(__name__)
dash_app1 = analysis_app.create_app(server, '/analysis/')
dash_app2 = scenario_app.create_app(server, '/scenario/')
dash_app3 = sim_app.create_app(server, '/sim/')


@server.route('/')
def serve_index():
    """Serve the landing page at the site root."""
    return INDEX_HTML


# Set up Dash apps at appropriate nodes within Flask app

@server.route('/analysis/')
def serve_analysis():
    """Serve the Analysis dashboard."""
    return flask.redirect('/dash1')


@server.route('/scenario/')
def serve_scenario():
    """Serve the Scenario Modelling dashboard."""
    return flask.redirect('/dash2')


@server.route('/sim/')
def serve_sim():
    """Serve the Simulation dashboard."""
    return flask.redirect('/dash3')


app = Dispatcher(
    server,
    {
        '/dash1': dash_app1.server,
        '/dash2': dash_app2.server,
        '/dash3': dash_app3.server
    }
)


def main():
    """Serve the flask app."""
    run_simple('localhost', 8080, app, use_reloader=True, use_debugger=True)


INDEX_HTML = f"""<html>
    <head>
        <title>CUH Covid Multi-Dashboard</title>
        <link rel="stylesheet" type="text/css" href="{dbc.themes.BOOTSTRAP}">
    </head>
    <body style='margin: 1rem'>
        <h1>CUH Covid Multi-Dashboard</h1>
        <div class="d-flex">
            <a href="/analysis/" class="btn btn-primary btn-lg p-3 me-2">Patient Analysis Dashboard</a>
            <a href="/scenario/" class="btn btn-primary btn-lg p-3 mx-2">Scenario Modelling Dashboard</a>
            <a href="/sim/" class="btn btn-primary btn-lg p-3 ms-2">Simulation Dashboard</a>
        </div>
    </body>
</html>
"""
