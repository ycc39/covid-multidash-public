"""Generate arrival numbers with a Gamma model."""

from math import exp, gamma
from random import uniform

import pandas as pd


def gam_pdf(x: float, shape: float, scale: float):
    """Gamma pdf, copied from random module documentation.

    The `shape` parameter affects skewness (skewness = 2/sqrt(`shape`))
    while the `scale` parameter "stretches out" the distribution.
    The `shape` parameter is defined such that `shape`=1 returns
    an exponential distribution while `shape` -> infty approaches a
    normal distribution.

    The mode (x value giving the maximum y value) of
    the distribution is `(shape-1)*scale`.
    """
    num = x ** (shape - 1) * exp(-x / scale)
    denom = gamma(shape) * scale ** shape
    return num / denom


def gamma_curve(start_date: str, shape: float, scale: float,
                max_val: float, max_jitter: float, min_jitter: float, n_days=180):
    """Generate a patient arrival project based on a gamma function.

    Args:
        start_date (str): Start date corresponding to x = 0 in the gamma function.
        shape (float): Shape parameter of the gamma function.
        scale (float): Scale parameter of the gamma function
        max_val (float): Maximum value of the (scaled) gamma function.
        max_jitter (float): Maximum jitter to add to the gamma function.
        min_jitter (float): Minimum jitter to add to the gamma function, should be negative.
        n_days (int, optional): Number of days to generate for the scenario. Defaults to 180.

    Returns:
        dict: Number of arrivals to generate for each date.
    """
    date0 = pd.Timestamp(start_date)
    xs = list(range(n_days))  # 0 to n_days - 1, inclusive
    dates = pd.date_range(date0, periods=n_days, freq='D')

    # Where is the mode of the gamma distribution?
    gam_mode = (shape - 1) * scale  # See Wikipedia page

    # Apply gam_pdf and scale to match set maximum value
    ys = [
        max(
            0,
            int(round(
                gam_pdf(x, shape, scale) /
                gam_pdf(gam_mode, shape, scale) * max_val
                + uniform(min_jitter, max_jitter)
            ))
        )
        for x in xs
    ]

    return pd.DataFrame(
        dict(Date=dates, n_Arrivals=ys)
    )
