"""Declare callbacks for the Scenario Modelling dashboard."""

from base64 import b64decode
from io import BytesIO

import dash_bootstrap_components as dbc
import pandas as pd
import plotly.graph_objects as go
from dash import dash, dcc, html
from dash.dependencies import Input, Output, State

from .gen_download_file import gen_download_file
from .modelling import gamma_curve


def register_callbacks(app: dash.Dash):
    """Register callbacks for a dash app.

    Args:
    - app (dash.Dash): The app to which callbacks are
      registered.
    """
    print('Registering callbacks')

    @app.callback(
        Output('arrival-data', 'data'),
        Output('upload-status', 'children'),
        Output('input-sim-start-date', 'date'),
        Output('button-download', 'disabled'),
        Output('button-download', 'color'),
        Input('file-upload', 'contents'),
        State('file-upload', 'filename'),

        prevent_initial_call=True
    )
    def upload_config(content: str, filename: str):
        """Get arrival data from uploaded file.

        The arrival data is expected to start in Cell D2 (with 1 header row),
        with dates in column D and daily arrivals in column E.

        Args:
            content (str): The file as a base64 encoded string with metadata.
            filename (str): The filename of the uploaded file.

        Returns:
           1. The parsed arrival data
           2. Status message.
           3. Start date of the arrival data, as default value for simulation start date.
           4-5. Enables the download button for generating a Scenarios file and changes the color.

           If parsing fails, tuple indexes 1, 3, and 4 return dash.no_update.
        """
        print('Callback: upload_config')
        if content is None:
            return dash.no_update, dash.no_update, dash.no_update, dash.no_update, dash.no_update
        try:
            if not filename.endswith('.xlsx'):
                raise Exception('Uploaded file not an .xlsx file.')

            # file_contents: base64 encoded string
            _, file_contents = content.split(',')
            df = pd.read_excel(
                BytesIO(b64decode(file_contents)),
                sheet_name='Patient Arrivals',
                header=1,
                usecols='D:E'
            )

            return (
                df.to_dict('records'),
                dbc.Alert(
                    [
                        html.I(className="bi bi-check-circle-fill me-2"),
                        f' Successfully read and parsed {filename}.'
                    ],
                    color="success",
                    className="d-flex align-items-start",
                    dismissable=True
                ),
                df.Date.min(),
                False,
                'primary'
            )
        except Exception as exception:
            return (
                dash.no_update,
                dbc.Alert(
                    [
                        html.I(className="bi bi-x-octagon-fill me-2"),
                        dcc.Markdown(
                            str(exception),
                            # undo margin added by last <p> in dcc Markdown element
                            style={'margin-bottom': '-1em'}
                        ),
                    ],
                    color="danger",
                    className="d-flex align-items-start",
                    dismissable=False
                ),
                dash.no_update,
                dash.no_update,
                dash.no_update
            )

    @app.callback(
        Output('input-start-date', 'date'),
        Input('arrival-data', 'data'),
        prevent_initial_call=True
    )
    def set_start_date(data):
        """Set the "x = 0" date for the fitted scenario curves."""

        print('Callback: set_start_date')
        if data is None:
            return dash.no_update
        df = pd.DataFrame(data)
        return max(df.Date)

    # Not a callback, but called by the three callback functions below.
    def gen_scenario(start_date, shape, scale, max_val, max_jitter, min_jitter):
        """Generate daily arrival forecast for a scenario."""
        if start_date is None or shape is None or scale is None or max_val is None:
            return None
        if max_jitter is None:
            max_jitter = 0
        if min_jitter is None:
            min_jitter = 0
        df = gamma_curve(start_date, shape, scale,
                         max_val, max_jitter, min_jitter)
        return df.to_dict('records')

    @app.callback(
        Output('scenario-data-1', 'data'),
        Input('input-start-date', 'date'),
        Input('input-shape', 'value'),
        Input('input-scale', 'value'),
        Input('input-max-1', 'value'),
        Input('input-max-jitter-1', 'value'),
        Input('input-min-jitter-1', 'value'),

        prevent_initial_call=True
    )
    def scenario_1(start_date, shape, scale, max_val, max_jitter, min_jitter):
        print('callback scenario_1')
        return gen_scenario(start_date, shape, scale, max_val, max_jitter, min_jitter)

    @app.callback(
        Output('scenario-data-2', 'data'),
        Input('input-start-date', 'date'),
        Input('input-shape', 'value'),
        Input('input-scale', 'value'),
        Input('input-max-2', 'value'),
        Input('input-max-jitter-2', 'value'),
        Input('input-min-jitter-2', 'value'),

        prevent_initial_call=True
    )
    def scenario_2(start_date, shape, scale, max_val, max_jitter, min_jitter):
        print('callback scenario_2')
        return gen_scenario(start_date, shape, scale, max_val, max_jitter, min_jitter)

    @app.callback(
        Output('scenario-data-3', 'data'),
        Input('input-start-date', 'date'),
        Input('input-shape', 'value'),
        Input('input-scale', 'value'),
        Input('input-max-3', 'value'),
        Input('input-max-jitter-3', 'value'),
        Input('input-min-jitter-3', 'value'),

        prevent_initial_call=True
    )
    def scenario_3(start_date, shape, scale, max_val, max_jitter, min_jitter):
        print('callback scenario_3')
        return gen_scenario(start_date, shape, scale, max_val, max_jitter, min_jitter)

    @app.callback(
        Output('graph-daily', 'figure'),
        Input('arrival-data', 'data'),
        Input('scenario-data-1', 'data'),
        Input('scenario-data-2', 'data'),
        Input('scenario-data-3', 'data'),

        prevent_initial_call=True
    )
    def plot_scenarios(data: list, data1: list, data2: list, data3: list):
        """Plots the daily actual and modelled patient arrivals.

        Args:
            - data (list): Dash store content for actual patient arrivals.
            - data1 (list): Dash store content for patient arrivals in the optimistic scenario.
            - data2 (list): Dash store content for actual patient arrivals in
                            the base-case scenario.
            - data3 (list): Dash store content for actual patient arrivals in
                            the worst-case scenario.

        Returns:
            go.Figure: The Figure object containing the plot.
        """

        print('callback plot_scenarios')
        fig = go.Figure()
        if data is not None:
            df = pd.DataFrame(data)
            fig.add_bar(x=df.Date, y=df['# Arrivals'], name='Actual')
        if data1 is not None:
            df1 = pd.DataFrame(data1)
            fig.add_scatter(x=df1.Date, y=df1.n_Arrivals,
                            mode='lines', name='Optimistic case')
        if data2 is not None:
            df2 = pd.DataFrame(data2)
            fig.add_scatter(x=df2.Date, y=df2.n_Arrivals,
                            mode='lines', name='Base case')
        if data3 is not None:
            df3 = pd.DataFrame(data3)
            fig.add_scatter(x=df3.Date, y=df3.n_Arrivals,
                            mode='lines', name='Worst case')
        return fig
        # TODO: keep x limits when plot refreshed

    @app.callback(
        Output('content-download', 'data'),
        Input('button-download', 'n_clicks'),
        State('arrival-data', 'data'),
        State('scenario-data-1', 'data'),
        State('scenario-data-2', 'data'),
        State('scenario-data-3', 'data'),
        State('file-upload', 'contents'),
        State('input-sim-start-date', 'date'),

        prevent_initial_call=True
    )
    def download_scenarios(_, data: list, data1: list, data2: list, data3: list,
                           config_file_contents: str, sim_start_date: str):
        """Builds the scenario Excel file for download."""

        print('callback download_scenarios')
        if data is None or data1 is None or data2 is None or data3 is None \
                or config_file_contents is None:
            return dash.no_update
        return gen_download_file(data, data1, data2, data3, config_file_contents, sim_start_date)
