"""Main module for the Scenario dashboard.
"""

import dash_bootstrap_components as dbc
from dash import dash, dcc, html
from flask import Flask

from .callbacks import register_callbacks
from .info import info


def create_app(server: Flask, path: str) -> dash.Dash:
    """Create a dash dashboard and associate it with the given path.

    Args:
        server (Flask): The server where the dashboard will be hosted.
        path (str): The server path to host the dashboard from, e.g. "/app/".

    Returns:
        dash.Dash: The dashboard.
    """
    app = dash.Dash(
        'CUH Covid Multi-dashboard',
        server=server,
        url_base_pathname=path,
        external_stylesheets=[dbc.themes.BOOTSTRAP, dbc.icons.BOOTSTRAP],
        suppress_callback_exceptions=True
    )
    app.layout = layout(app)
    register_callbacks(app)
    return app


INPUT_STYLE = {
    'width': '150px',
    'display': 'inline',
    'margin': '0 3em 0 1em'
}


def layout(app: dash.Dash):
    """Generate the app layout for the Scenario Modelling dashboard.

    Args:
        app (dash.Dash): The dashboard to populate with HTML and Dash elements.

    Returns:
    - An html.Div.
    """
    return html.Div(
        [
            html.H1('CUH Covid Multi-Dashboard: Scenario Modelling'),
            html.Div(
                [
                    html.Div(
                        [
                            html.A(
                                [dbc.Button(
                                    'Home', className='btn btn-danger'
                                )],
                                href='/'
                            ),
                            dcc.Upload(
                                [dbc.Button('Upload Config File',
                                            className='btn btn-primary')],
                                id='file-upload',
                                multiple=False,
                                style={'width': 'fit-content',
                                       'display': 'inline'}
                            ),
                        ],
                        className='d-inline-flex gap-1'
                    ),

                    dcc.Store(id='arrival-data'),
                    dcc.Store(id='scenario-data-1'),
                    dcc.Store(id='scenario-data-2'),
                    dcc.Store(id='scenario-data-3'),

                    dcc.Loading(
                        dbc.Alert(
                            [
                                html.I(
                                    className='bi bi-exclamation-triangle-fill me-2'),
                                'No file uploaded yet.'
                            ],
                            color="warning",
                            className="d-flex align-items-start"
                        ),
                        id='upload-status',
                    )
                ],
                className='d-grid gap-2 mb-2'
            ),
            dbc.Tabs(
                [
                    dbc.Tab(
                        [
                            html.Div(
                                [
                                    html.Label('Start Date: '),
                                    dcc.DatePickerSingle(
                                        id='input-start-date',
                                        style=INPUT_STYLE,
                                        display_format='YYYY-MM-DD'
                                    ),
                                    html.Label('Shape: '),
                                    dbc.Input(
                                        id='input-shape',
                                        style=INPUT_STYLE,
                                        type='number',
                                        min=1,
                                        value=3
                                    ),
                                    html.Label('Scale: '),
                                    dbc.Input(
                                        id='input-scale',
                                        style=INPUT_STYLE,
                                        type='number',
                                        min=0,
                                        value=20
                                    )
                                ],
                                className='mt-1 mb-1'
                            ),
                            html.Div(
                                [
                                    html.Label(
                                        'Maximum (Optimistic Case): ',
                                        htmlFor='input-max-1'
                                    ),
                                    dbc.Input(
                                        id='input-max-1',
                                        style=INPUT_STYLE,
                                        type='number',
                                        min=0,
                                        value=10
                                    ),
                                    html.Label(
                                        'Maximum (Base Case): ',
                                        htmlFor='input-max-2'
                                    ),
                                    dbc.Input(
                                        id='input-max-2',
                                        style=INPUT_STYLE,
                                        type='number',
                                        min=0,
                                        value=20
                                    ),
                                    html.Label(
                                        'Maximum (Worst Case): ',
                                        htmlFor='input-max-3'
                                    ),
                                    dbc.Input(
                                        id='input-max-3',
                                        style=INPUT_STYLE,
                                        type='number',
                                        min=0,
                                        value=40
                                    )
                                ],
                                className='mt-1 mb-1'
                            ),
                            html.Div(
                                [
                                    html.Label(
                                        'Max jitter (Optimistic Case): ',
                                        htmlFor='input-max-jitter-1'
                                    ),
                                    dbc.Input(
                                        id='input-max-jitter-1',
                                        style=INPUT_STYLE,
                                        type='number',
                                        value=1,
                                        min=0
                                    ),
                                    html.Label(
                                        'Max jitter (Base Case): ',
                                        htmlFor='input-max-jitter-2'
                                    ),
                                    dbc.Input(
                                        id='input-max-jitter-2',
                                        style=INPUT_STYLE,
                                        type='number',
                                        value=2,
                                        min=0
                                    ),
                                    html.Label(
                                        'Max jitter (Worst Case): ',
                                        htmlFor='input-max-jitter-3'
                                    ),
                                    dbc.Input(
                                        id='input-max-jitter-3',
                                        style=INPUT_STYLE,
                                        type='number',
                                        value=5,
                                        min=0
                                    )
                                ],
                                className='mt-1 mb-1'
                            ),
                            html.Div(
                                [
                                    html.Label(
                                        'Min jitter (Optimistic Case): ',
                                        htmlFor='input-min-jitter-1'
                                    ),
                                    dbc.Input(
                                        id='input-min-jitter-1',
                                        style=INPUT_STYLE,
                                        type='number',
                                        value=0,
                                        max=0
                                    ),
                                    html.Label(
                                        'Min jitter (Base Case): ',
                                        htmlFor='input-min-jitter-2'
                                    ),
                                    dbc.Input(
                                        id='input-min-jitter-2',
                                        style=INPUT_STYLE,
                                        type='number',
                                        value=-2,
                                        max=0
                                    ),
                                    html.Label(
                                        'Min jitter (Worst Case): ',
                                        htmlFor='input-min-jitter-3'
                                    ),
                                    dbc.Input(
                                        id='input-min-jitter-3',
                                        style=INPUT_STYLE,
                                        type='number',
                                        value=-5,
                                        max=0
                                    )
                                ],
                                className='mt-1 mb-1'
                            ),
                            dcc.Graph(id='graph-daily'),
                            html.Div(
                                [
                                    html.Label(
                                        'Scenario Start Date: ',
                                        htmlFor='input-sim-start-date'
                                    ),
                                    dcc.DatePickerSingle(
                                        id='input-sim-start-date',
                                        style=INPUT_STYLE,
                                        display_format='YYYY-MM-DD'
                                    ),
                                    dbc.Button(
                                        'Download file',
                                        id='button-download',
                                        disabled=True,
                                        color='secondary',
                                    ),
                                    dcc.Download(id='content-download')
                                ]
                            )
                        ],
                        label='Modelling Tool',
                        active_label_style={
                            'background-color': 'lightblue', 'font-weight': 'bold'}
                    ),
                    dbc.Tab(
                        info(app),
                        label='Info',
                        active_label_style={
                            'background-color': 'lightblue', 'font-weight': 'bold'}
                    ),
                ]
            )
        ],
        className='d-grid',
        style={'margin': '1rem', 'width': '1600px'}
    )
