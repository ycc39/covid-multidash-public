"""Definitions for the Info tab in the Scenario dashboard."""

from dash import dash, dcc


def info(app: dash.Dash):
    """Populate the Info tab in the Scenario dashboard."""
    return dcc.Markdown(
        f"""This dashboard generates scenario files for simulation purposes. The dashboard
accepts Excel files as input and expects a table of daily arrivals starting at cell **D2** in
the worksheet "**Patient Arrivals**":

![Screenshot]({app.get_asset_url('actual_arrivals_example.png')})

The number of daily patient arrivals for each modelled scenario is based on a Gamma function
with four parameters: the start date, shape and scale parameters, and a maximum value. A lower
shape parameter skews more "mass" of the function to the left, whereas the scale parameter
stretches or compresses the entire Gamma function along the time axis.  There are **three** inputs
for the maximum value, one for each scenario.  Finally, inputs are provided for adding random jitter
to each scenario, with separate inputs dictating the maximum amount the generated daily arrivals may
exceed/fall below the Gamma function value.

![Screenshot]({app.get_asset_url('scenarios_screenshot.png')})

The generated scenarios can be downloaded by clicking the "Download File" button.
The "Scenario Start Date" input controls the start date of the scenarios in the generated
Excel file. Note that for dates where actual patient arrival data is available from the
source Excel file, the actual values will **override** any generated values from the scenarios.

Currently, the end date for the scenarios is coded as **180** days from the value of the
scenario start date parameter (assuming that this is after the last date with actual patient
arrival data)."""
    )
