"""Provides functions for graphing occupancy results for the simulation model.
"""

import pandas as pd
from dash import dcc
import plotly.graph_objs as go


def make_plot(
        occupancy: pd.DataFrame,
        actuals: pd.DataFrame | None,  # from dash Store
        colname: str,
        title: str
):
    """Plots occupancy data from a pandas DataFrame, i.e.
    output from `pctiles.pctiles(simConfig.run())`.
    If `actuals` is not None, then adds actual occupancy
    numbers to this plot.

    Args:
    - occupancy (pd.DataFrame): Simulated occupancy data.
    Contains the 5th and 9th percentiles of simulated daily
    bed occupancies, as well as the mean.
    - actuals (pd.DataFrame | None): Actual occupancy data, or None.
    - colname: the column name for the occupancy data (within `actuals`).
    - title (str): The title of the plot.

    Returns:
        dcc.Graph: Plot of the occupancy data
    """
    # https://plotly.com/python/continuous-error-bars/#filling-between-two-traces
    go_list = [
        go.Scatter(
            name='Mean',
            x=occupancy.Date,
            y=occupancy.m,
            mode='lines',
            line={'color': 'rgb(31, 119, 180)'},
        ),
        go.Scatter(
            name='95th percentile',
            x=occupancy.Date,
            y=occupancy.q95,
            mode='lines',
            marker={'color': '#444'},
            line={'width': 0},
            showlegend=False
        ),
        go.Scatter(
            name='5th percentile',
            x=occupancy.Date,
            y=occupancy.q5,
            marker={'color': '#444'},
            line={'width': 0},
            mode='lines',
            fillcolor='rgba(68, 68, 68, 0.3)',
            fill='tonexty',
            showlegend=False
        )
    ]

    # Plot actuals if present
    if actuals is not None and colname in actuals:
        x_bar = actuals.Date
        y_bar = actuals[colname]
        go_list.append(
            go.Bar(
                name='Actual',
                x=x_bar,
                y=y_bar
            )
        )

    fig = go.Figure(go_list)
    fig.update_layout(title=title, hovermode='x')
    fig.update_xaxes(range=[min(occupancy.Date), max(occupancy.Date)])
    return dcc.Graph(figure=fig, style={'height': '30rem'})
