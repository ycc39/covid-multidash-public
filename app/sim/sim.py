"""Implements the actual simulation for the Simulation dashboard."""
import re
from collections import namedtuple
from random import random, uniform
from typing import Callable

import numpy as np
import pandas as pd
import simpy
from reliability import Distributions as Dists
from scipy.stats import rv_discrete
from simpy.core import Infinity, SimTime


def sim_main(
        set_progress: Callable, n_reps: int, scenario: str, stepdown: float,
        data_params, data_scenarios, data_hourly
):
    """Main simulation function.

    Args:
    - set_progress (Callable): Function to display simulation progress.
      Expects a 3-tuple of strings.
    - n_reps (int): Number of simulation replications to run.
    - scenario (str): Name of scenario to run, corresponding to a column
      in the Scenarios data frame.
    - stepdown (float): Number of days after admission where a patient in GIM is
      moved from a red bed to a green bed.
    - data_params: Dash Store contents (a dict) for patient parameters.
    - data_scenarios: Dash store contents (a dict) for patient arrival scenarios.
    - data_hourly: Dash Store contents (a dict) for the patient arrival hourly distribution.

    Returns:
        A Dash.Div containing the simulation results, with plots and a download button.
    """
    params = PatientParams(data_params)
    epoch = pd.to_datetime(pd.DataFrame(data_scenarios).loc[:, 'Date']).min()
    daily_arrivals = pd.DataFrame(data_scenarios).loc[:, scenario].to_list()

    # Construct discrete distribution from data_hourly input
    hourly_dist = pd.DataFrame(data_hourly).loc[:, 'Frequency'].to_list()
    x = np.arange(24)
    x, hourly_dist = remove_zero_prob(x, hourly_dist)
    hourly_dist = rv_discrete(values=(x, hourly_dist))

    gim_log = []
    icu_log = []
    red_log = []

    for i in range(n_reps):
        env = Environment(params, epoch, daily_arrivals, hourly_dist, stepdown)
        env.run(until=float(len(daily_arrivals)))

        def daily_max(df: pd.DataFrame):
            df.timestamp = pd.to_timedelta(df.timestamp, unit='day') + epoch
            df = df.resample('D', on='timestamp').max().ffill().value
            return df

        env.gim_log = daily_max(env.gim_log)
        env.icu_log = daily_max(env.icu_log)
        env.red_log = daily_max(env.red_log)

        gim_log.append(env.gim_log)
        icu_log.append(env.icu_log)
        red_log.append(env.red_log)
        set_progress((str(i), str(n_reps), f'{i}/{n_reps}'))

    gim_log = pd.concat(gim_log, axis=1, ignore_index=True)
    icu_log = pd.concat(icu_log, axis=1, ignore_index=True)
    red_log = pd.concat(red_log, axis=1, ignore_index=True)

    def mean_quantiles(df: pd.DataFrame):
        return pd.DataFrame(
            dict(
                Date=df.index,
                q5=df.quantile(0.05, axis=1).to_list(),
                m=df.mean(axis=1).to_list(),
                q95=df.quantile(0.95, axis=1).to_list()
            )
        )

    gim_log = mean_quantiles(gim_log)
    icu_log = mean_quantiles(icu_log)
    red_log = mean_quantiles(red_log)

    return [
        gim_log.to_dict('records'),
        icu_log.to_dict('records'),
        red_log.to_dict('records')
    ]


class PatientParams:
    """Data class for patient parameters including survival probabilities,
    ICU admission rate, pre-ICU probability of ICU patients, and length-of-stay
    distributions.
    """

    def __init__(self, data_params: dict) -> None:
        """Constructor

        Args:
            data_params (dict): dash.Store representation of the patient parameters.
        """
        df_survival = pd.DataFrame(data_params['survival'])
        df_los = pd.DataFrame(data_params['los'])
        icu_rate = float(data_params['icu_rate'])
        pre_icu_rate = float(data_params['pre_icu_rate'])

        # Note that we will assume the correct order of the rows in df_survival and df_los
        survival = df_survival['Survival Rate'].to_list()
        survival = namedtuple('SurvivalTuple', 'gim indirect direct')(*survival)

        los_dists = df_los['Fitted Distribution'].to_list()
        los_dists = [parse_los(x) for x in los_dists]
        los_dists = namedtuple('LosTuple', 'gim_survived gim_died pre_icu icu post_icu')(*los_dists)

        self.survival = survival
        self.los_dists = los_dists
        self.icu_rate = icu_rate
        self.pre_icu_rate = pre_icu_rate


def remove_zero_prob(x, p):
    """Remove elements x[i] and p[i] where p[i] == 0.

    Args:
        x: list or list-like.
        p: list or list-like of floats.

    Returns:
        The lists x and p with the matching elements removed.
    """
    x_new = [x[i] for i in range(len(x)) if p[i] != 0]
    p_new = [p[i] for i in range(len(x)) if p[i] != 0]
    return x_new, p_new


def parse_los(los_str: str):
    """Create a distribution object from a string. Supports syntax
    from the `reliability` package.

    Args:
        los_str (str): string representing the distribution as returned
        by the `param_title_long()` function of a
        `reliability.Distributions.___` object

    Raises:
        Exception: raised if the string cannot be parsed as
        a known distribution.

    Returns:
        `reliability.Distributions.___` object
    """
    # tokenize, remove '' tokens, and strip leading/trailing spaces
    tokens = [x.strip() for x in re.split('[(=,)]', los_str) if x != '']

    if tokens[0] == 'Weibull Distribution' and len(tokens) == 7:
        return Dists.Weibull_Distribution(*[float(tokens[i]) for i in [2, 4, 6]])
    if tokens[0] == 'Weibull Distribution' and len(tokens) == 5:
        return Dists.Weibull_Distribution(*[float(tokens[i]) for i in [2, 4]])

    if tokens[0] == 'Gamma Distribution' and len(tokens) == 7:
        return Dists.Gamma_Distribution(*[float(tokens[i]) for i in [2, 4, 6]])
    if tokens[0] == 'Gamma Distribution' and len(tokens) == 5:
        return Dists.Gamma_Distribution(*[float(tokens[i]) for i in [2, 4]])

    if tokens[0] == 'Lognormal Distribution' and len(tokens) == 7:
        return Dists.Lognormal_Distribution(*[float(tokens[i]) for i in [2, 4, 6]])
    if tokens[0] == 'Lognormal Distribution' and len(tokens) == 5:
        return Dists.Lognormal_Distribution(*[float(tokens[i]) for i in [2, 4]])

    if tokens[0] == 'Exponential Distribution' and len(tokens) == 5:
        return Dists.Exponential_Distribution(*[float(tokens[i]) for i in [2, 4]])
    if tokens[0] == 'Exponential Distribution' and len(tokens) == 5:
        return Dists.Exponential_Distribution(float(tokens[2]))

    raise Exception(f'Unsupported distribution: {los_str}')


def new_log():
    """Create a new pandas DataFrame for logging bed occupancy in the simulation.

    Returns:
        pd.DataFrame: An empty dataframe with columns 'timestamp' and 'value'.
    """
    return pd.DataFrame(
        dict(
            timestamp=pd.Series(dtype='float'),
            value=pd.Series(dtype='int')
        )
    )


def log_append(log: pd.DataFrame, timestamp: SimTime, value: int):
    """Add a row to an occupancy log.

    Args:
        log (pd.DataFrame): The occupancy log.
        timestamp (float): Time which the new occupancy is recorded.
        value (int): The new occupancy level.
    """
    # SimTime can be int or float, we want only floats
    new_row = pd.DataFrame(dict(timestamp=[float(timestamp)], value=[value]))
    return pd.concat([log, new_row], ignore_index=True)


class Environment(simpy.Environment):
    """Simulation environment for the hospital COVID simulation.
    """

    def __init__(
            self,
            params: PatientParams,
            epoch: pd.Timestamp,
            daily_arrivals: list,
            hourly_dist: rv_discrete,
            stepdown: float
    ):
        super().__init__()

        # Simulation parameters
        self.params = params
        self.epoch = epoch
        self.daily_arrivals = daily_arrivals
        self.hourly_dist = hourly_dist
        self.stepdown = stepdown

        # Resources
        # Since we want to compute resource demand, we will
        # assume infinite capacity.
        self.gim_beds = simpy.Resource(self, Infinity)
        self.icu_beds = simpy.Resource(self, Infinity)
        self.red_beds = simpy.Resource(self, Infinity)

        # Occupancy logs
        self.gim_log = log_append(new_log(), self.now, self.gim_beds.count)
        self.icu_log = log_append(new_log(), self.now, self.icu_beds.count)
        self.red_log = log_append(new_log(), self.now, self.red_beds.count)

        # Arrival generator
        self.process(arrival_generator(self))


def arrival_generator(env: Environment):
    """Generates patients for the simulation `Environment`.

    Args:
        env (Environment): The simulation `Environment`.

    Yields:
        simpy.events.Process: Each new patient's `Process`.
    """

    # Note that new arrivals are generated at midnight each day.
    # The `new_arrival` process is responsible for setting the
    # actual admission time of each patient within the day.
    # All time units in the Environment are in days.

    for n_arrivals in env.daily_arrivals:  # for each day
        for _ in range(n_arrivals):  # for each arrival in the day
            env.process(new_arrival(env))
        yield env.timeout(1.)  # Wait until next day


def new_arrival(env: Environment):
    """Generator process for a new patient arrival.

    Args:
        env (Environment): The simulation environment.

    Yields:
        A simpy Event.
    """
    # Get the time of day of the arrival (as a random number between 0 and 1)
    arrival_offset = env.hourly_dist.rvs() / 24.0 + uniform(0.0, 1.0 / 24.0)
    yield env.timeout(arrival_offset)

    if random() < env.params.icu_rate:
        yield from icu_process(env)
    else:
        yield from gim_process(env)


def gim_process(env: Environment):
    """Generator process for a GIM-only patient.

    Args:
        env (Environment): The simulation environment.

    Yields:
         A simpy Event.
    """
    is_survived = random() < env.params.survival.gim
    if is_survived:
        los = env.params.los_dists.gim_survived.random_samples(1)[0]
    else:
        los = env.params.los_dists.gim_died.random_samples(1)[0]

    red_time = min(los, env.stepdown)

    with env.gim_beds.request() as main_request:
        yield main_request
        env.gim_log = log_append(env.gim_log, env.now, env.gim_beds.count)

        if red_time > 0.0:
            with env.red_beds.request() as red_request:
                yield red_request
                env.red_log = log_append(env.red_log, env.now, env.red_beds.count)
                yield env.timeout(red_time)
            # release red bed
            env.red_log = log_append(env.red_log, env.now, env.red_beds.count)

        remaining_time = los - red_time
        if remaining_time > 0.0:
            yield env.timeout(remaining_time)
    # release GIM bed
    env.gim_log = log_append(env.gim_log, env.now, env.gim_beds.count)


def icu_process(env: Environment):
    """Generator process for an ICU patient.

    Args:
        env (Environment): The simulation environment.

    Yields:
         A simpy Event.
    """
    has_pre_icu = random() < env.params.pre_icu_rate
    if has_pre_icu:
        los_pre_icu = env.params.los_dists.pre_icu.random_samples(1)[0]

        # Pre-ICU stay
        red_time = min(los_pre_icu, env.stepdown)
        with env.gim_beds.request() as main_request:
            yield main_request
            env.gim_log = log_append(env.gim_log, env.now, env.gim_beds.count)

            if red_time > 0.0:
                with env.red_beds.request() as red_request:
                    yield red_request
                    env.red_log = log_append(env.red_log, env.now, env.red_beds.count)
                    yield env.timeout(red_time)
                # release red bed
                env.red_log = log_append(env.red_log, env.now, env.red_beds.count)

            remaining_time = los_pre_icu - red_time
            if remaining_time > 0.0:
                yield env.timeout(remaining_time)
        # release GIM bed
        env.gim_log = log_append(env.gim_log, env.now, env.gim_beds.count)
    else:
        los_pre_icu = 0.0

    # ICU stay
    los_icu = env.params.los_dists.icu.random_samples(1)[0]
    with env.icu_beds.request() as request:
        yield request
        env.icu_log = log_append(env.icu_log, env.now, env.icu_beds.count)
        yield env.timeout(los_icu)
    # release ICU bed
    env.icu_log = log_append(env.icu_log, env.now, env.icu_beds.count)

    # Post-ICU stay (if patient survived)
    icu_survival_rate = env.params.survival.indirect \
        if has_pre_icu else env.params.survival.direct
    if random() < icu_survival_rate:
        los_post_icu = env.params.los_dists.post_icu.random_samples(1)[0]
        red_time = min(los_post_icu, env.stepdown - los_post_icu - los_pre_icu)
        red_time = max(red_time, 0.0)

        with env.gim_beds.request() as main_request:
            yield main_request
            env.gim_log = log_append(env.gim_log, env.now, env.gim_beds.count)

            if red_time > 0.0:
                with env.red_beds.request() as red_request:
                    yield red_request
                    env.red_log = log_append(env.red_log, env.now, env.red_beds.count)
                    yield env.timeout(red_time)
                # release red bed
                env.red_log = log_append(env.red_log, env.now, env.red_beds.count)

            remaining_time = los_post_icu - red_time
            if remaining_time > 0.0:
                yield env.timeout(remaining_time)
        # release GIM bed
        env.gim_log = log_append(env.gim_log, env.now, env.gim_beds.count)
