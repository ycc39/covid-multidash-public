"""Declare callbacks for the Simulate dashboard."""

from base64 import b64decode
from io import BytesIO
from typing import Optional

import dash_bootstrap_components as dbc
import openpyxl as xl
import pandas as pd
import plotly.express as px
from dash import dash, dcc, html
from dash.dependencies import Input, Output, State

from .plotting import make_plot
from .sim import sim_main

ZOOM_X_MSG = html.P(
    'To zoom in on the x axis only, click and drag in a horizontal line. '
    'To pan, click and drag along the x axis labels, just below the plot. '
    'Double-clicking on the plot resets the zoom level.'
)


def register_callbacks(app: dash.Dash):
    """Register callbacks for a dash app.

    Args:
    - app (dash.Dash): The app to which callbacks are
      registered.
    """
    print('Registering callbacks')

    @app.callback(
        Output('store-patient-params', 'data'),
        Output('store-scenarios', 'data'),
        Output('store-hourly-dist', 'data'),
        Output('upload-status', 'children'),
        Output('button-simulate', 'disabled'),
        Output('button-simulate', 'color'),
        Input('file-upload', 'contents'),
        State('file-upload', 'filename'),
        prevent_initial_call=True
    )
    def upload_config(content: str, filename: str):
        """Get simulation configuration from uploaded Excel file.
        Configuration includes patient parameters, daily arrivals for
        each scenario, and the hourly distribution of arrivals for each day.

        Args:
        - content (str): The file as a base64 encoded string with metadata.
        - filename (str): The filename of the uploaded file.

        Returns:
        """
        print('Callback: upload_config')
        if content is None:
            return dash.no_update, dash.no_update, dash.no_update, dash.no_update
        try:
            if not filename.endswith('.xlsx'):
                raise Exception('Uploaded file not an .xlsx file.')
            # file_contents: base64 encoded string
            _, file_contents = content.split(',')
            b_io = BytesIO(b64decode(file_contents))

            # Patient particulars
            df_survival = pd.read_excel(
                b_io,
                sheet_name='Patient Parameters',
                header=1,
                nrows=3,
                usecols='A:B'
            )
            df_los = pd.read_excel(
                b_io,
                sheet_name='Patient Parameters',
                header=1,
                nrows=5,
                usecols='D:E'
            )
            wb = xl.load_workbook(b_io, read_only=True)
            icu_rate = float(wb['Patient Parameters']['B7'].value)
            pre_icu_rate = float(wb['Patient Parameters']['B8'].value)
            patient_params = dict(
                survival=df_survival.to_dict('records'),
                los=df_los.to_dict('records'),
                icu_rate=icu_rate,
                pre_icu_rate=pre_icu_rate
            )

            # Scenarios
            df_scenarios = pd.read_excel(
                b_io,
                sheet_name='Scenarios'
                # Default = start at A1
            )

            # Hourly distribution
            df_hourly = pd.read_excel(
                b_io,
                sheet_name='Patient Arrivals',
                header=1,
                nrows=24,
                usecols='A:B'
            )

            return (
                patient_params,
                df_scenarios.to_dict('records'),
                df_hourly.to_dict('records'),
                dbc.Alert(
                    [
                        html.I(className="bi bi-check-circle-fill me-2"),
                        f' Successfully read and parsed {filename}.'
                    ],
                    color="success",
                    className="d-flex align-items-start",
                    dismissable=True
                ),
                False, 'primary'
            )
        except Exception as exception:
            return (
                dash.no_update,
                dash.no_update,
                dash.no_update,
                dbc.Alert(
                    [
                        html.I(className="bi bi-x-octagon-fill me-2"),
                        dcc.Markdown(
                            str(exception),
                            # undo margin added by last <p> in dcc Markdown element
                            style={'margin-bottom': '-1em'}
                        ),
                    ],
                    color="danger",
                    className="d-flex align-items-start",
                    dismissable=False
                ),
                dash.no_update,
                dash.no_update
            )

    @app.callback(
        Output('table-survival', 'data'),
        Output('table-los', 'data'),
        Output('span-icu-rate', 'children'),
        Output('span-pre-icu-rate', 'children'),
        Input('store-patient-params', 'data'),

        prevent_initial_call=True
    )
    def show_patient_data(data: dict):
        return (
            data['survival'],
            data['los'],
            f"{round(data['icu_rate'] * 100, 2)}%",
            f"{round(data['pre_icu_rate'] * 100, 2)}%",
        )

    @app.callback(
        Output('graph-scenarios', 'figure'),
        Input('store-scenarios', 'data'),

        prevent_initial_call=True
    )
    def plot_scenarios(data: dict):
        df = pd.DataFrame(data)
        fig = px.line(df, 'Date', ['Optimistic', 'Base', 'Worst'])
        fig.update_layout(legend_title_text='Scenario')
        fig.update_yaxes(title_text='Daily Arrivals')
        return fig

    @app.callback(
        Output('graph-hourly', 'figure'),
        Input('store-hourly-dist', 'data'),

        prevent_initial_call=True
    )
    def plot_hourlies(data: dict):
        df = pd.DataFrame(data)
        fig = px.bar(df, 'Hour', 'Frequency')
        fig.update_yaxes(tickformat='.0%')
        return fig

    @app.callback(
        Output('store-sim-result', 'data'),
        Output('sim-results-container', 'className'),
        Input('button-simulate', 'n_clicks'),
        State('input-num-reps', 'value'),
        State('input-scenario', 'value'),
        State('input-stepdown', 'value'),
        State('store-patient-params', 'data'),
        State('store-scenarios', 'data'),
        State('store-hourly-dist', 'data'),

        background=True,
        running=[
            (Output('button-simulate', 'disabled'), True, False),
            (Output('button-simulate', 'color'), 'secondary', 'primary'),
            (
                Output('sim-progress-div', 'className'),
                'd-flex gap-2',
                'd-flex gap-2 visually-hidden'
            ),
            (
                Output('sim-results-container', 'className'),
                'd-grid gap-2 visually-hidden',
                'd-grid gap-2'
            )
        ],
        progress=[
            Output("sim-progress", "value"),
            Output("sim-progress", "max"),
            Output("sim-progress-text", "children")
        ],
        prevent_initial_call=True
    )
    def sim(
            set_progress, _, n_reps, scenario, stepdown,
            data_params, data_scenarios, data_hourly
    ):
        """_summary_

        Args:
            set_progress (Callable):
                Callback to update the progress bar and text.
            _ (int):
                Ignored.
            n_reps (int):
                Number of simulation replications to run.
            scenario (str):
                Name of scenario to run.
            stepdown (float):
                Red patients are moved from red GIM beds to green GIM beds after
                this many days in the hospital.
            data_params (dict):
                dash Store contents (i.e. dict) containing patient parameters.
            data_scenarios (_type_):
                dash Store contents (i.e. dict) containing patient arrival scenarios,
                i.e. the number of daily arrivals.
            data_hourly (_type_):
                dash Store contents (i.e. dict) containing the hourly distribution
                of patient arrivals.

        Returns:
            _type_: _description_
        """
        print('Callback: sim')
        set_progress(('0', str(n_reps), f'0/{n_reps}'))
        return (
            sim_main(
                set_progress, int(n_reps), scenario, float(stepdown),
                data_params, data_scenarios, data_hourly
            ),
            'd-grid gap-2'
        )

    @app.callback(
        Output('sim-results-plots', 'children'),
        Input('store-sim-result', 'data'),
        Input('store-actuals', 'data'),

        prevent_initial_call=True
    )
    def show_results(results: list, actuals: Optional[dict] = None):
        """Generate a html.Div to show simulation results.

        Args:
        - results (list): dash.Store contents containing the simulation results.
        - actuals (dict, optional): dash.Store contents containing actual occupancy levels.
        Defaults to None.

        Returns:
            html.Div: A Div containing the simulation results and dashboard controls.
        """
        print('Callback: show_results')
        if results is None:
            return dash.no_update
        gim_results, icu_results, red_results = [
            pd.DataFrame(x) for x in results]
        df_actuals = pd.DataFrame(actuals) if (actuals is not None) else None
        return [
            ZOOM_X_MSG,
            make_plot(gim_results, df_actuals,
                      'Actual GIM', 'GIM bed occupancy'),
            make_plot(icu_results, df_actuals,
                      'Actual ICU', 'ICU bed occupancy'),
            make_plot(red_results, df_actuals, 'Ops GIM', 'Red bed occupancy')
        ]

    @app.callback(
        Output('store-actuals', 'data'),
        Output('upload-actuals-status', 'children'),
        Input('file-actuals-upload', 'contents'),
        State('file-actuals-upload', 'filename'),

        prevent_initial_call=True
    )
    def upload_actuals(content: str, filename: str):
        print('Callback: upload_actuals')
        if content is None:
            return dash.no_update, dash.no_update
        try:
            if not filename.endswith('.xlsx'):
                raise Exception('Uploaded file not an .xlsx file.')
            # file_contents: base64 encoded string
            _, file_contents = content.split(',')
            b_io = BytesIO(b64decode(file_contents))

            df = pd.read_excel(  # default: first sheet, header in row "1" (0 in pandas)
                b_io,
                usecols='A:E'  # Date, ICU, GIM, Ops ICU, Ops GIM
            )
            return (
                df.to_dict('records'),
                dbc.Alert(
                    [
                        html.I(className="bi bi-check-circle-fill me-2"),
                        f' Successfully read and parsed {filename}.'
                    ],
                    color="success",
                    className="d-flex align-items-start",
                    dismissable=True
                )
            )
        except Exception as exception:
            return (
                dash.no_update,
                dbc.Alert(
                    [
                        html.I(className="bi bi-x-octagon-fill me-2"),
                        dcc.Markdown(
                            str(exception),
                            # undo margin added by last <p> in dcc Markdown element
                            style={'margin-bottom': '-1em'}
                        ),
                    ],
                    color="danger",
                    className="d-flex align-items-start",
                    dismissable=False
                )
            )

    @app.callback(
        Output('sim-results-download', 'data'),
        Input('download-button', 'n_clicks'),
        State('store-sim-result', 'data'),
        State('input-scenario', 'value'),
        State('store-actuals', 'data'),

        prevent_initial_call=True
    )
    def gen_xlsx(_, sim_result, scenario, actuals=None):
        print('Callback: gen_xlsx')

        if sim_result is None:
            return dash.no_update
        sim_result_gim, sim_result_icu, sim_result_red = sim_result  # unpack

        cols = dict(q5='5th pctile', m='mean', q95='95th pctile')

        sim_result_gim = pd.DataFrame(
            sim_result_gim).set_index('Date').rename(cols)
        sim_result_icu = pd.DataFrame(
            sim_result_icu).set_index('Date').rename(cols)
        sim_result_red = pd.DataFrame(
            sim_result_red).set_index('Date').rename(cols)

        if actuals is not None:
            actuals = pd.DataFrame(actuals).set_index('Date')
            sim_result = pd.concat(
                [sim_result_gim, sim_result_icu, sim_result_red, actuals],
                axis=1,
                keys=['GIM beds', 'ICU beds', 'Red beds', 'Actual occupancy']
            )
        else:
            sim_result = pd.concat(
                [sim_result_gim, sim_result_icu, sim_result_red],
                axis=1,
                keys=['GIM beds', 'ICU beds', 'Red beds']
            )

        b_io = BytesIO()
        sim_result.to_excel(b_io, f'{scenario} Case')
        return dcc.send_bytes(
            b_io.getvalue(),
            filename=f'sim_results_{scenario}.xlsx',
            type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        )
