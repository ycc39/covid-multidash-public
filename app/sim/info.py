"""Definitions for the Info tab in the Simulation dashboard."""
from dash import dash, dcc


def info(app: dash.Dash):
    return dcc.Markdown(
        f"""This
dashboard contains controls for running SimPy simulations based on
patient parameters and scenarios computed/built using the other two
dashboards in this multi-dashboard.

## Scenarios Excel file

The primary input of the simulator
is an Excel .xlsx file with at least three sheets: "Patient Parameters",
"Patient Arrivals", and "Scenarios".

The "Patient Parameters" worksheet must contain:

- A table of survival rates, at cell A2
- A table of length-of-stay distributions, at cell D2
- The ICU admission rate of patients, in cell B7
- The percentage of ICU patients with pre-ICU stays, in cell B8

![Screenshot]({app.get_asset_url('patient_parameters_example.png')})

The "Patient Arrivals" worksheet must contain a table at cell A2
containing the hourly arrival distribution of patients.

![Screenshot]({app.get_asset_url('hourlies_example.png')})

Finally, the "Scenarios" worksheet must contain at table at cell A1
containing the daily patient arrival numbers for each of three scenarios:
Optimistic, Base, and Worst.

![Screenshot]({app.get_asset_url('scenarios_example.png')})

## Simulation controls

The simulation controls include inputs for the number of simulation replications,
the choice between the Optimistic, Base, and Worst-case scenarios,
and a "red bed step-down" threshold.  This threshold is defined such that any
Covid patient with a total length of stay greater than the threshold is moved from a
"red" GIM bed to a "green" GIM bed provided that they are not in an ICU bed.

## Plotting actual GIM/ICU bed occupancies

Users may upload an .xlsx file containing actual GIM/ICU bed occupancies for
plotting. The file should contain a table starting at cell A1 with columns
`Date`, `Actual ICU`, `Actual GIM`, and `Ops GIM` (for red bed occupancy).
However, the app is designed to simply skip plotting actual occupancies
for any missing columns.

## Save to .xlsx

The simulation results can be downloaded to an .xlsx file, containing the
mean and 5th/95 percentiles of the simulated daily bed occupancies for the
selected scenario.  Actual bed occupancy data can also be outputted to the
.xlsx file if previously uploaded to the dashboard.  The actuals .xlsx file
must contain a table with at least 4 columns in its first worksheet:
Date, Actual ICU, Actual GIM, and Ops GIM.  Here, "Ops" corresponds to "red"
bed occupancy.

## Other tabs

This dashboard contains several tabs for visualisation of the uploaded Scenario
file data.  The tabs are similar to those in the Analysis dashboard (see the
Info tab in the Analysis dashboard for more information).
"""
    )
