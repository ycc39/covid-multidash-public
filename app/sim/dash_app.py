"""Main module for the Simulation dashboard.
"""

import dash_bootstrap_components as dbc
import diskcache
from dash import DiskcacheManager, dash, dcc, html
from dash.dash_table import DataTable
import dash.dash_table.Format as dtFormat
from flask import Flask

from .callbacks import register_callbacks
from .info import info


def create_app(server: Flask, path: str) -> dash.Dash:
    """Create a dash dashboard and associate it with the given path.

    Args:
        server (Flask): The server where the dashboard will be hosted.
        path (str): The server path to host the dashboard from, e.g. "/app/".

    Returns:
        dash.Dash: The dashboard.
    """
    cache = diskcache.Cache("./cache")
    background_callback_manager = DiskcacheManager(cache)
    app = dash.Dash(
        'CUH Covid Multi-dashboard',
        server=server,
        url_base_pathname=path,
        external_stylesheets=[dbc.themes.BOOTSTRAP, dbc.icons.BOOTSTRAP],
        suppress_callback_exceptions=True,
        background_callback_manager=background_callback_manager
    )
    app.layout = layout(app)
    register_callbacks(app)
    return app


def layout(app: dash.Dash) -> html.Div:
    """Generates the app layout for the Simulation dashboard.

    Args:
        app (dash.Dash): The dashboard to populate with HTML and Dash elements.

    Returns:
        html.Div: The top-level div for the dashboard.
    """

    return html.Div(
        [
            html.H1('CUH Covid Multi-Dashboard: Simulation'),
            html.Div(
                [
                    html.Div(
                        [
                            html.A(
                                [
                                    dbc.Button('Home', className='btn btn-danger')
                                ],
                                href='/'
                            ),
                            dcc.Upload(
                                [
                                    dbc.Button('Upload scenarios file', color='primary')
                                ],
                                id='file-upload',
                                multiple=False
                            )
                        ],
                        className='d-flex gap-2'
                    ),
                    dcc.Loading(
                        dbc.Alert(
                            [
                                html.I(className='bi bi-exclamation-triangle-fill me-2'),
                                'No Scenarios file uploaded yet.'
                            ],
                            color="warning",
                            className="d-flex align-items-start"
                        ),
                        id='upload-status'
                    )
                ]
            ),
            dcc.Store(id='store-patient-params'),
            dcc.Store(id='store-scenarios'),
            dcc.Store(id='store-hourly-dist'),
            dcc.Store(id='store-sim-result'),
            dcc.Store(id='store-actuals'),
            dbc.Tabs(
                [
                    dbc.Tab(
                        TAB_CHILDREN_SIMULATE,
                        tab_id='tab-simulate',
                        label='Simulate!',
                        active_label_style={'background-color': 'lightblue', 'font-weight': 'bold'}
                    ),
                    dbc.Tab(
                        TAB_CHILDREN_PATIENT,
                        tab_id='tab-patient',
                        label='View Patient Parameters',
                        active_label_style={'background-color': 'lightblue', 'font-weight': 'bold'}
                    ),
                    dbc.Tab(
                        TAB_CHILDREN_SCENARIOS,
                        tab_id='tab-scenarios',
                        label='View Scenarios',
                        active_label_style={'background-color': 'lightblue', 'font-weight': 'bold'}
                    ),
                    dbc.Tab(
                        TAB_CHILDREN_HOURLY,
                        tab_id='tab-hourly',
                        label='View Hourly Arrival Distribution',
                        active_label_style={'background-color': 'lightblue', 'font-weight': 'bold'}
                    ),
                    dbc.Tab(
                        info(app),
                        tab_id='tab-info',
                        label='Info',
                        active_label_style={'background-color': 'lightblue', 'font-weight': 'bold'}
                    )
                ]
            )
        ],
        className='d-grid',
        style=dict(margin='1rem', width='1600px')
    )


TAB_CHILDREN_SIMULATE = [
    html.Div(
        [
            html.Label(
                html.B('Number of replications: '),
                htmlFor='input-num-reps'
            ),
            dbc.Input(
                id='input-num-reps',
                type='number',
                min=0, step=1, value=30,
                style={'width': '6rem'}
            ),

            html.Label(html.B('Scenario: '), htmlFor='input-num-reps'),
            dbc.Select(
                id='input-scenario',
                options=[
                    dict(label='Optimistic', value='Optimistic'),
                    dict(label='Base', value='Base'),
                    dict(label='Worst', value='Worst'),
                ],
                value='Base',
                style={'width': '12rem'},
            ),

            html.Label(
                html.B('Red bed step-down threshold: '),
                htmlFor='input-red-stepdown'
            ),
            dbc.Input(
                id='input-stepdown',
                type='number',
                min=0, value=10,
                style={'width': '6rem'},
            ),

            dbc.Button('SIMULATE!', id='button-simulate', color='secondary', disabled=True)
        ],
        className='d-flex gap-2 mt-2 mb-3'
    ),
    html.Div(
        [
            html.B('Running: '),
            html.Progress(id='sim-progress', value='0'),
            html.Span('0/??', id='sim-progress-text')
        ],
        id='sim-progress-div',
        className='d-flex gap-2 visually-hidden',
    ),
    html.Div(
        [
            html.Div(
                [
                    dcc.Upload(
                        dbc.Button('Upload Actual occupancies', color='primary'),
                        id='file-actuals-upload'
                    ),
                    dbc.Button('Download .xlsx', id='download-button', color='primary')
                ],
                className='d-flex gap-3',
                id='sim-results-controls'
            ),
            dcc.Download(id='sim-results-download'),
            dcc.Loading(
                dbc.Alert(
                    [
                        html.I(className='bi bi-exclamation-triangle-fill me-2'),
                        'No Actuals file uploaded yet.'
                    ],
                    color="warning",
                    className="d-flex align-items-start"
                ),
                id='upload-actuals-status'
            ),
            html.Div(id='sim-results-plots')
        ],
        id='sim-results-container',
        className='d-grid gap-2 visually-hidden'
    )
]

percentage = dtFormat.Format(
    nully=0,
    precision=2,
    scheme=dtFormat.Scheme.percentage
)

TAB_CHILDREN_PATIENT = html.Div([
    html.Div([
        html.Div(html.B('Survival Rates:')),
        DataTable(
            columns=[
                dict(name="Stay Type", id="Stay Type"),
                dict(name="Survival Rate", id="Survival Rate", type='numeric', format=percentage)
            ],
            id='table-survival',
            style_table=dict(width='500px'),
            style_header={'font-weight': 'bold'}
        )
    ]),
    html.Div([
        html.B('ICU admission rate (all patients): '),
        html.Span(id='span-icu-rate')
    ]),
    html.Div([
        html.B('Pre-ICU rate (ICU patients): '),
        html.Span(id='span-pre-icu-rate')
    ]),
    html.Div([
        html.Div(html.B('Length of Stay (LoS):')),
        DataTable(
            columns=[
                dict(name="LoS Type", id="LoS Type"),
                dict(name="Fitted Distribution", id="Fitted Distribution")
            ],
            id='table-los',
            style_table=dict(width='500px'),
            style_header={'font-weight': 'bold'}
        )
    ])
], className='d-grid gap-2')

TAB_CHILDREN_SCENARIOS = dcc.Graph(id='graph-scenarios')

TAB_CHILDREN_HOURLY = dcc.Graph(id='graph-hourly', style=dict(width='720px'))
